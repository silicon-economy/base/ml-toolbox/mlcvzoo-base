# MLCVZoo Base

The MLCVZoo is an SDK for simplifying the usage of various (machine learning driven)
computer vision algorithms. The package **mlcvzoo_base** provides the base modules
that are defining the MLCVZoo API. Furthermore, it includes modules that allow to handle
and process the data structures of the MLCVZoo, as well as providing modules for
running evaluations / calculation of metrics.

Further information about the MLCVZoo can be found [here](documentation).

## Install
`
pip install mlcvzoo-base
`

## Usage


### Environment Variables
| Variable | Description                                                                                                                                                                                                                                                   | Optional |
|----------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------|
| MLCVZOO_MODEL_REGISTRY_PATH        | Path to the yaml configuration file of the model registry entries to use for the mlcvzoo ModelRegistry initialization. An example configuration file can be found in config/mlcvzoo_registry.yaml. If the varialbe is not set, a default configuration is used. | yes      |


## Technology stack

- Python

## Developer Documentation (WIP)
A [Sphinx-based developer documentation](https://silicon-economy.pages.fraunhofer.de/base/ml-toolbox/mlcvzoo-base/index.html) is hosted via Gitlab Pages and accessibly for authorized SE Gitlab users.
