.. mlcvzoo-base documentation master file, created by
   sphinx-quickstart on Thu Aug  3 14:10:28 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to mlcvzoo-base's documentation!
========================================

The MLCVZoo is an SDK for simplifying the usage of various (machine learning driven)
computer vision algorithms. The package **mlcvzoo_base** provides the base modules
that are defining the MLCVZoo API. Furthermore, it includes modules that allow to handle
and process the data structures of the MLCVZoo, as well as providing modules for
running evaluations / calculation of metrics.

.. toctree::
   :maxdepth: 2
   :caption: Get Started

   get_started/installation.rst
   get_started/quick_run.rst
   get_started/faq.rst

   modules/modules.rst





.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
