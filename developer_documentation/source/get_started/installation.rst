============
Installation
============
The following section describes all possible ways to install mlcvzoo-base.

-------------
As dependency
-------------
The `mlcvzoo-base python package <https://pypi.org/project/mlcvzoo-base/>`_ is available on pypi and can be installed using any of your preferred ways to install python packages.

**Using pip:**

.. code-block:: bash

   pip install mlcvzoo-base

-------------
Dev setup
-------------
`uv <https://github.com/astral-sh/uv>`_ is used to setup the python environment. It is recommended to create a dedicated virtual environment for the project.

Prerequisite:

* Install uv

**Install mlcvzoo-base dependencies**

.. code-block::

   cd mlcvzoo-base
   # Optional: Set your requirements-file via the environment variable REQUIREMENTS_FILE, which will be consumed by the build.sh script.
   bash build.sh

**Generate requirements file with uv**

.. code-block:: bash

   bash scripts/lock-dependencies.sh
