Before opening a new issue, make sure to search for keywords in the issues

<!--
Implementation issues are used to break-up a large piece of work into small, discrete tasks that can
move independently through the build workflow steps. They're typically used to populate a Feature
Epic. Once created, an implementation issue is usually refined in order to populate and review the
implementation plan and weight.
-->

## Why are we doing this work

A brief explanation of the why, not the what or how. Assume the reader doesn't know the
background and won't have time to dig-up information from comment threads.



## Relevant links
Information that the developer might need to refer to when implementing the issue.

<!--
- [Design Issue](https://gitlab.com/gitlab-org/gitlab/-/issues/<id>)
  - [Design 1](https://gitlab.com/gitlab-org/gitlab/-/issues/<id>/designs/<image>.png)
  - [Design 2](https://gitlab.com/gitlab-org/gitlab/-/issues/<id>/designs/<image>.png)
- [Similar implementation](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/<id>)
-->


## Non-functional requirements

- [ ] Documentation:
- [ ] Testing:

/label ~"type::implementation"
