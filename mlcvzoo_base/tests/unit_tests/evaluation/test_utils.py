# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

import logging
import os
from typing import Dict, List

from mlcvzoo_base.api.data.annotation import BaseAnnotation
from mlcvzoo_base.api.data.types import ImageType
from mlcvzoo_base.data_preparation.annotation_handler import AnnotationHandler
from mlcvzoo_base.evaluation.geometric.model_evaluation import (
    evaluate_with_precomputed_data,
)
from mlcvzoo_base.evaluation.geometric.utils import (
    create_fp_fn_images,
    generate_metric_table,
)
from mlcvzoo_base.tests.unit_tests.test_template import TestTemplate

logger = logging.getLogger(__name__)


class TestEvaluationUtils(TestTemplate):
    def test_create_fp_fn_images(self) -> None:
        annotation_handler = AnnotationHandler(
            yaml_config_path=os.path.join(
                self.project_root,
                "test_data/test_AnnotationHandler/"
                "annotation-handler_pascal-voc_evaluation.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
        )

        gt_annotations: List[BaseAnnotation] = annotation_handler.parse_annotations_from_xml()

        annotation_handler = AnnotationHandler(
            yaml_config_path=os.path.join(
                self.project_root,
                "test_data/test_AnnotationHandler/"
                "annotation-handler_pascal-voc_evaluation_fp_fn_images.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
        )

        predicted_annotations: List[BaseAnnotation] = (
            annotation_handler.parse_annotations_from_xml()
        )

        iou_thresholds = [0.5]

        # Compute Metrics
        model_metrics = evaluate_with_precomputed_data(
            model_specifier="test_od_evaluation_with_precomputed",
            gt_annotations=gt_annotations,
            predictions_list=[
                annotation.get_bounding_boxes(include_segmentations=True)
                for annotation in predicted_annotations
            ],
            iou_thresholds=iou_thresholds,
            mapper=annotation_handler.mapper,
        )

        metric_table = generate_metric_table(
            metrics_dict=model_metrics.metrics_dict,
            iou_threshold=iou_thresholds[0],
        )

        logger.info(
            metric_table.table,
        )

        fp_fn_image_dict: Dict[str, Dict[str, ImageType]] = create_fp_fn_images(
            metrics_image_info_dict=model_metrics.metrics_image_info_dict
        )

        # NOTE: There is no fp-fn image for the truck image, since there were only TP.
        #       There is no fp-fn image for the truck class in the car image, since there
        #       were only TP.
        expected_image_paths = [
            "metric_image_info_0_person/person_ID_2_GT_2_TP_1_FP_0_FN_1_.jpg",
            "metric_image_info_1_truck/person_ID_2_GT_1_TP_0_FP_0_FN_1_.jpg",
            "metric_image_info_2_car/cars_ID_0_GT_3_TP_2_FP_1_FN_1_.jpg",
            "metric_image_info_2_car/empty_ID_1_GT_0_TP_0_FP_1_FN_0_.jpg",
        ]

        assert list(fp_fn_image_dict.keys()) == expected_image_paths
