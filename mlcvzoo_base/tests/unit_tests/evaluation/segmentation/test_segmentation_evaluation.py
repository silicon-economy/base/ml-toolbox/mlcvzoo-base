# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

import json
import logging
import os
from typing import Any, Dict, List
from unittest import main

import cv2
from tqdm import tqdm

from mlcvzoo_base.api.data.annotation import BaseAnnotation
from mlcvzoo_base.api.data.segmentation import Segmentation
from mlcvzoo_base.api.model import SegmentationModel
from mlcvzoo_base.configuration.model_config import ModelConfig
from mlcvzoo_base.data_preparation.annotation_handler import AnnotationHandler
from mlcvzoo_base.evaluation.geometric.configuration import TensorboardLoggingConfig
from mlcvzoo_base.evaluation.geometric.data_classes import METRIC_DICT_TYPE
from mlcvzoo_base.evaluation.geometric.metrics_computation import (
    EvaluationContexts,
    MetricsComputation,
)
from mlcvzoo_base.evaluation.geometric.metrics_logging import output_evaluation_results
from mlcvzoo_base.evaluation.geometric.structs import MetricTypes
from mlcvzoo_base.models.model_registry import ModelRegistry
from mlcvzoo_base.tests.unit_tests.test_template import TestTemplate
from mlcvzoo_base.utils import draw_on_image
from mlcvzoo_base.utils.common_utils import CustomJSONEncoder
from mlcvzoo_base.utils.file_utils import ensure_dir

logger = logging.getLogger(__name__)


class TestSegmentationEvaluation(TestTemplate):
    def test_segmentation_evaluation_with_precomputed(self) -> None:
        annotation_handler = AnnotationHandler(
            yaml_config_path=os.path.join(
                self.project_root,
                "test_data/test_AnnotationHandler/annotation-handler_coco_test.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
        )

        gt_annotations: List[BaseAnnotation] = annotation_handler.parse_annotations_from_coco()

        predictions_list = [annotation.segmentations for annotation in gt_annotations]

        iou_thresholds = [0.5]
        show_visualization = False
        if show_visualization:
            for annotation, predictions in zip(gt_annotations, predictions_list):
                image = cv2.imread(annotation.image_path)

                logger.info("Draw for image: %s" % annotation.image_path)

                for class_identifier in annotation_handler.mapper.create_class_identifier_list():
                    cv2.namedWindow(f"{class_identifier}", cv2.WINDOW_NORMAL)
                    cv2.resizeWindow(
                        winname=f"{class_identifier}",
                        height=750,
                        width=750,
                    )
                    gt_segmentations = [
                        s
                        for s in annotation.segmentations
                        if s.class_identifier == class_identifier
                    ]

                    prediction_segmentations = [
                        s for s in predictions if s.class_identifier == class_identifier
                    ]

                    if len(gt_segmentations) > 0 or len(prediction_segmentations) > 0:
                        image = draw_on_image(
                            frame=image,
                            rgb_colors=[(255, 255, 255)] * 100,
                            segmentations=gt_segmentations,
                            draw_caption=False,
                            thickness=5,
                        )

                        image = draw_on_image(
                            frame=image,
                            rgb_colors=[(255, 0, 0)] * 100,
                            segmentations=prediction_segmentations,
                            draw_caption=False,
                            thickness=3,
                        )
                        cv2.imshow(f"{class_identifier}", image)
                        cv2.waitKey(1)
            cv2.waitKey(0)

        # Compute Metrics
        model_metrics = MetricsComputation(
            model_specifier="test_segmentation_evaluation_with_precomputed",
            gt_annotations=gt_annotations,
            predictions_list=predictions_list,
            iou_thresholds=iou_thresholds,
            mapper=annotation_handler.mapper,
            evaluation_context=EvaluationContexts.SEGMENTATION,
        ).compute_metrics()

        output_evaluation_results(
            model_metrics=model_metrics,
            iou_thresholds=iou_thresholds,
            tensorboard_logging=TensorboardLoggingConfig(
                tensorboard_dir=os.path.join(
                    self.project_root,
                    "test_output/tensorboard_evaluation/test_segmentation_evaluation_with_precomputed",
                )
            ),
        )

        predicted_metrics_file_dir = os.path.join(
            self.project_root,
            "test_output",
            "evaluation",
            "test_segmentation_evaluation_with_precomputed",
            "predicted_metrics",
        )

        wanted_metrics_file_dir = os.path.join(
            self.project_root,
            "test_data/test_segmentation_evaluation/",
            "wanted_metrics",
        )

        predicted_metrics_file_name = f"metrics-dict_precomputed.json"
        wanted_metrics_file_name = f"wanted_{predicted_metrics_file_name}"

        predicted_metrics_file_path = os.path.join(
            predicted_metrics_file_dir, predicted_metrics_file_name
        )

        wanted_metrics_file_path = os.path.join(wanted_metrics_file_dir, wanted_metrics_file_name)

        ensure_dir(file_path=predicted_metrics_file_path, verbose=True)

        with open(file=predicted_metrics_file_path, mode="w") as predicted_metrics_file:
            logger.debug("Write predicted metrics-dict to: %s", predicted_metrics_file_path)
            json.dump(
                obj=model_metrics.metrics_dict,
                fp=predicted_metrics_file,
                indent=2,
                cls=CustomJSONEncoder,
            )

        wanted_metrics: Dict[float, Dict[str, Dict[str, Any]]]
        with open(file=wanted_metrics_file_path, mode="r") as wanted_metrics_file:
            logger.debug("Read wanted metrics-dict from: %s", wanted_metrics_file_path)
            wanted_metrics_dict = json.load(fp=wanted_metrics_file)

        assert TestSegmentationEvaluation.__check_metrics_equal(
            metrics_dict=model_metrics.metrics_dict,
            wanted_metrics_dict=wanted_metrics_dict,
        )

    def test_segmentation_evaluation_model_based(self) -> None:
        annotation_handler = AnnotationHandler(
            yaml_config_path=os.path.join(
                self.project_root,
                "test_data/test_AnnotationHandler/annotation-handler_coco_segmentation_gt.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
        )

        gt_annotations: List[BaseAnnotation] = annotation_handler.parse_annotations_from_coco()

        model_registry = ModelRegistry()

        model_config = ModelConfig(
            class_type="read_from_file_segmentation",
            constructor_parameters={
                "from_yaml": os.path.join(
                    self.project_root,
                    "test_data/test_ReadFromFileObjectDetectionModel/read-from-file_coco_segmentation_test.yaml",
                ),
            },
        )

        iou_thresholds = [0.1]

        model: Model[PredictionType, ConfigurationType, DataType]  # type: ignore
        model = model_registry.init_model(
            model_config=model_config,
            string_replacement_map=self.string_replacement_map,
        )

        if not isinstance(model, SegmentationModel):
            raise ValueError(
                "This evaluation can only be used with models that "
                "inherit from 'mlcvzoo.api.model.ObjectDetectionModel'"
            )

        predictions_list: List[List[Segmentation]] = []

        process_bar = tqdm(
            gt_annotations,
            desc=f"Compute metrics",
        )

        for index, gt_annotation in enumerate(process_bar):
            # Every ObjectDetectionModel returns bounding boxes
            _, predicted_segmentations = model.predict(data_item=gt_annotation.image_path)
            predictions_list.append(predicted_segmentations)

        show_visualization = False
        if show_visualization:
            for annotation, predictions in zip(gt_annotations, predictions_list):
                image = cv2.imread(annotation.image_path)

                logger.info("Draw for image: %s" % annotation.image_path)

                for class_identifier in model.mapper.create_class_identifier_list():
                    cv2.namedWindow(f"{class_identifier}", cv2.WINDOW_NORMAL)
                    cv2.resizeWindow(
                        winname=f"{class_identifier}",
                        height=750,
                        width=750,
                    )
                    gt_segmentations = [
                        s
                        for s in annotation.segmentations
                        if s.class_identifier == class_identifier
                    ]

                    prediction_segmentations = [
                        s for s in predictions if s.class_identifier == class_identifier
                    ]

                    if len(gt_segmentations) > 0 or len(prediction_segmentations) > 0:
                        image = draw_on_image(
                            frame=image,
                            rgb_colors=[(255, 255, 255)] * 100,
                            segmentations=gt_segmentations,
                            draw_caption=False,
                            thickness=3,
                        )

                        image = draw_on_image(
                            frame=image,
                            rgb_colors=[(255, 0, 0)] * 100,
                            segmentations=prediction_segmentations,
                            draw_caption=False,
                            thickness=3,
                        )
                        cv2.imshow(f"{class_identifier}", image)
                        cv2.waitKey(1)
            cv2.waitKey(0)

        model_metrics = MetricsComputation(
            model_specifier=model.unique_name,
            iou_thresholds=iou_thresholds,
            gt_annotations=gt_annotations,
            predictions_list=predictions_list,
            mapper=model.mapper,
            evaluation_context=EvaluationContexts.SEGMENTATION,
        ).compute_metrics()

        output_evaluation_results(
            model_metrics=model_metrics,
            iou_thresholds=iou_thresholds,
            tensorboard_logging=TensorboardLoggingConfig(
                tensorboard_dir=os.path.join(
                    self.project_root,
                    "test_output/tensorboard_evaluation/test_segmentation_evaluation_model_based",
                )
            ),
        )

        predicted_metrics_file_dir = os.path.join(
            self.project_root,
            "test_output",
            "evaluation",
            "test_segmentation_evaluation",
            "predicted_metrics",
        )

        wanted_metrics_file_dir = os.path.join(
            self.project_root,
            "test_data/test_segmentation_evaluation/",
            "wanted_metrics",
        )

        predicted_metrics_file_name = f"metrics-dict_model_based.json"
        wanted_metrics_file_name = f"wanted_{predicted_metrics_file_name}"

        predicted_metrics_file_path = os.path.join(
            predicted_metrics_file_dir, predicted_metrics_file_name
        )

        wanted_metrics_file_path = os.path.join(wanted_metrics_file_dir, wanted_metrics_file_name)

        ensure_dir(file_path=predicted_metrics_file_path, verbose=True)

        with open(file=predicted_metrics_file_path, mode="w") as predicted_metrics_file:
            logger.debug("Write predicted metrics-dict to: %s", predicted_metrics_file_path)
            json.dump(
                obj=model_metrics.metrics_dict,
                fp=predicted_metrics_file,
                indent=2,
                cls=CustomJSONEncoder,
            )

        wanted_metrics: Dict[float, Dict[str, Dict[str, Any]]]
        with open(file=wanted_metrics_file_path, mode="r") as wanted_metrics_file:
            logger.debug("Read wanted metrics-dict from: %s", wanted_metrics_file_path)
            wanted_metrics_dict = json.load(fp=wanted_metrics_file)

        assert TestSegmentationEvaluation.__check_metrics_equal(
            metrics_dict=model_metrics.metrics_dict,
            wanted_metrics_dict=wanted_metrics_dict,
        )

    @staticmethod
    def __check_metrics_equal(
        metrics_dict: METRIC_DICT_TYPE,
        wanted_metrics_dict: Dict,
    ) -> bool:
        for iou_threshold, iou_threshold_dict in metrics_dict.items():
            for bbox_size_type, bbox_size_type_dict in iou_threshold_dict.items():
                for class_name, metrics in bbox_size_type_dict.items():
                    if (
                        wanted_metrics_dict[str(iou_threshold)][bbox_size_type][class_name][
                            MetricTypes.TRUE_POSITIVES
                        ]
                        != metrics.TP
                    ):
                        return False
                    if (
                        wanted_metrics_dict[str(iou_threshold)][bbox_size_type][class_name][
                            MetricTypes.FALSE_POSITIVES
                        ]
                        != metrics.FP
                    ):
                        return False
                    if (
                        wanted_metrics_dict[str(iou_threshold)][bbox_size_type][class_name][
                            MetricTypes.PRECISION
                        ]
                        != metrics.PR
                    ):
                        return False
                    if (
                        wanted_metrics_dict[str(iou_threshold)][bbox_size_type][class_name][
                            MetricTypes.RECALL
                        ]
                        != metrics.RC
                    ):
                        return False
                    if (
                        wanted_metrics_dict[str(iou_threshold)][bbox_size_type][class_name][
                            MetricTypes.F1
                        ]
                        != metrics.F1
                    ):
                        return False
                    if (
                        wanted_metrics_dict[str(iou_threshold)][bbox_size_type][class_name][
                            MetricTypes.AP
                        ]
                        != metrics.AP
                    ):
                        return False

        return True


if __name__ == "__main__":
    # Run unittest main
    main()
