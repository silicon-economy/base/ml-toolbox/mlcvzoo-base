# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

import os

import cv2
import numpy as np
import pytest

from mlcvzoo_base.api.data.bounding_box import BoundingBox
from mlcvzoo_base.api.data.box import Box
from mlcvzoo_base.api.data.class_identifier import ClassIdentifier
from mlcvzoo_base.api.data.classification import Classification
from mlcvzoo_base.api.data.segmentation import Segmentation
from mlcvzoo_base.api.data.types import FrameShape
from mlcvzoo_base.utils.draw_utils import (
    determine_draw_parameters,
    draw_bbox_cv2,
    draw_classification_cv2,
    draw_on_image,
    draw_polygon_cv2,
    generate_detector_colors,
)


def compare_frames(frame, result_frame):
    import matplotlib.pyplot as plt

    fig, axes = plt.subplots(1, 2, figsize=(12, 5), gridspec_kw={"wspace": 0.3})
    axes[0].imshow(frame)
    axes[0].set_title("original")
    axes[1].imshow(result_frame)
    axes[1].set_title("drawn frame")
    plt.show()


@pytest.fixture
def frame_shape() -> FrameShape:
    return FrameShape(640, 480)


@pytest.fixture
def frame() -> np.ndarray:
    return np.zeros((480, 640, 3), dtype=np.uint8)


@pytest.fixture
def drawing_path(project_root):
    path = os.path.join(project_root, "test_data/images/drawing/")
    os.makedirs(path, exist_ok=True)
    return path


@pytest.fixture
def class_identifier() -> ClassIdentifier:
    return ClassIdentifier(1, "Test")


@pytest.mark.parametrize(
    "thickness_factor, font_scale_factor", [(0.01, 0.005), (0.51, 0.002), (0.005, 0.035)]
)
def test_determine_draw_parameters(frame_shape, thickness_factor, font_scale_factor):
    thickness, font_scale = determine_draw_parameters(
        frame_shape, thickness_factor, font_scale_factor
    )

    expected_thickness = np.ceil(min(frame_shape.width, frame_shape.height) * thickness_factor)
    expected_font_scale = min(frame_shape.width, frame_shape.height) * font_scale_factor

    assert thickness == expected_thickness
    assert font_scale == expected_font_scale


@pytest.mark.parametrize("num_classes", [1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
def test_generate_detector_colors(num_classes) -> None:
    colors = generate_detector_colors(num_classes)

    assert len(colors) == num_classes
    assert all(isinstance(color, tuple) and len(color) == 3 for color in colors)
    assert all(((0 <= val <= 255) for val in color) for color in colors)


def test_draw_on_image(class_identifier, frame, drawing_path):
    rgb_colors = [(255, 0, 0), (0, 255, 0)]
    box = Box(34, 67, 235, 465)
    bounding_boxes = [
        BoundingBox(
            box,
            class_identifier,
            score=0.8,
            content="Test",
        )
    ]
    result_frame = draw_on_image(frame, rgb_colors, bounding_boxes=bounding_boxes)
    image_path = os.path.join(drawing_path, "draw_on_image.png")
    reference_frame = cv2.imread(image_path)
    assert np.array_equal(result_frame, reference_frame)
    assert np.array_equal(result_frame, reference_frame)


def test_draw_classification_cv2(class_identifier, frame, drawing_path):
    classifications = [Classification(class_identifier, 0.95)]
    result_frame = draw_classification_cv2(frame, classifications)
    image_path = os.path.join(drawing_path, "classification_cv2.png")
    reference_frame = cv2.imread(image_path)
    assert result_frame.shape == reference_frame.shape
    assert np.array_equal(result_frame, reference_frame)


def test_draw_polygon_cv2(frame, drawing_path) -> None:
    polygon = [(100, 100), (200, 100), (200, 200), (100, 200)]
    result_frame = draw_polygon_cv2(frame, (255, 255, 0), polygon)
    image_path = os.path.join(drawing_path, "polygon_cv2.png")
    reference_frame = cv2.imread(image_path)
    assert result_frame.shape == reference_frame.shape
    assert np.array_equal(result_frame, reference_frame)


def test_draw_bbox_cv2(frame, drawing_path) -> None:
    box = Box(100, 100, 200, 200)
    result_frame = draw_bbox_cv2(frame, (255, 0, 0), box)
    image_path = os.path.join(drawing_path, "bbox_cv2.png")
    reference_frame = cv2.imread(image_path)
    assert result_frame.shape == reference_frame.shape
    assert np.array_equal(result_frame, reference_frame)


def test_drawing_manually(class_identifier, project_root) -> None:
    """
    This method is intended as a manual test environment for drawing on images. Hence,
    it does not test anything automatically.
    """
    draw_window = os.getenv("DRAW_WINDOW", False)

    input_image_dir = os.path.join(project_root, "test_data/images/drawing/")
    output_image_dir = os.path.join(project_root, "test_output/images/drawing/")

    os.makedirs(output_image_dir, exist_ok=True)
    images = [
        "test_image_600x400.png",
        "test_image_1200x1200.png",
        "test_image_1920x1080.png",
        "test_image_3840x2160.png",
    ]

    window_name = "Test Drawings"
    if draw_window:
        cv2.namedWindow(window_name, cv2.WINDOW_NORMAL)

    for image in images:
        image_path = os.path.join(input_image_dir, image)

        frame = cv2.imread(image_path)
        classifications = [Classification(class_identifier, 0.95)]
        predictions = [
            BoundingBox(
                Box(100, 100, 200, 200),
                class_identifier,
                score=0.8,
                content="Test",
            ),
            Segmentation(
                [(320, 110), (500, 140), (630, 220), (300, 210)],
                class_identifier,
                score=0.8,
                content="Test",
            ),
        ]
        rgb_colors = [(0, 0, 255), (0, 0, 0)]
        result_frame = frame.copy()
        result_frame = draw_on_image(
            result_frame,
            rgb_colors,
            predictions=predictions,
            classifications=classifications,
            draw_caption=True,
        )

        cv2.imwrite(os.path.join(output_image_dir, "drawn_" + image), result_frame)

        if draw_window:
            cv2.imshow(window_name, result_frame)
            cv2.waitKey(0)
        else:
            compare_frames(frame, result_frame)
