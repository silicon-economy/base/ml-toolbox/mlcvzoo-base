# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

import logging
from math import isclose
from typing import Any, Dict
from unittest import main

from mlcvzoo_base.api.data.class_identifier import ClassIdentifier
from mlcvzoo_base.api.data.segmentation import Segmentation
from mlcvzoo_base.api.data.types import polygon_equal
from mlcvzoo_base.api.structs import float_equality_precision
from mlcvzoo_base.tests.unit_tests.test_template import TestTemplate

logger = logging.getLogger(__name__)


class TestAPISegmentation(TestTemplate):
    @staticmethod
    def __create_dummy_segmentation() -> Segmentation:
        return Segmentation(
            polygon=[[0.0, 0.0], [100.0, 0.0], [100.0, 100.0], [0.0, 100.0]],
            class_identifier=ClassIdentifier(class_id=0, class_name="test"),
            model_class_identifier=ClassIdentifier(class_id=0, class_name="test"),
            score=1.0,
            difficult=False,
            occluded=False,
            background=False,
            content="",
        )

    def test_to_dict(self) -> None:
        dummy_segmentation: Segmentation = self.__create_dummy_segmentation()
        expected_dict: Dict = {
            "polygon": [[0.0, 0.0], [100.0, 0.0], [100.0, 100.0], [0.0, 100.0]],
            "class_identifier": {"class_id": 0, "class_name": "test"},
            "model_class_identifier": {"class_id": 0, "class_name": "test"},
            "score": 1.0,
            "content": "",
            "difficult": False,
            "occluded": False,
            "background": False,
        }

        assert dummy_segmentation.to_dict() == expected_dict

    def test_to_dict_without_box(self) -> None:
        dummy_segmentation: Segmentation = self.__create_dummy_segmentation()
        expected_dict: Dict = {
            "polygon": [[0.0, 0.0], [100.0, 0.0], [100.0, 100.0], [0.0, 100.0]],
            "class_identifier": {"class_id": 0, "class_name": "test"},
            "model_class_identifier": {"class_id": 0, "class_name": "test"},
            "score": 1.0,
            "content": "",
            "difficult": False,
            "occluded": False,
            "background": False,
        }

        assert dummy_segmentation.to_dict() == expected_dict

    def test_to_dict_raw(self) -> None:
        dummy_segmentation: Segmentation = self.__create_dummy_segmentation()
        expected_dict: Dict = {
            "polygon": [[0.0, 0.0], [100.0, 0.0], [100.0, 100.0], [0.0, 100.0]],
            "class_identifier": dummy_segmentation.class_identifier,
            "model_class_identifier": dummy_segmentation.model_class_identifier,
            "score": 1,
            "content": "",
            "difficult": False,
            "occluded": False,
            "background": False,
        }

        dummy_segmentation_dict = dummy_segmentation.to_dict(raw_type=True)

        assert dummy_segmentation_dict["class_identifier"] == expected_dict["class_identifier"]
        assert dummy_segmentation_dict["content"] == expected_dict["content"]
        assert dummy_segmentation_dict["difficult"] == expected_dict["difficult"]
        assert (
            dummy_segmentation_dict["model_class_identifier"]
            == expected_dict["model_class_identifier"]
        )
        assert dummy_segmentation_dict["occluded"] == expected_dict["occluded"]
        assert dummy_segmentation_dict["background"] == expected_dict["background"]
        assert polygon_equal(dummy_segmentation_dict["polygon"], expected_dict["polygon"])
        assert isclose(
            dummy_segmentation_dict["score"],
            expected_dict["score"],
            abs_tol=float_equality_precision,
        )

    def test_to_dict_reduced(self) -> None:
        dummy_segmentation: Segmentation = self.__create_dummy_segmentation()
        expected_dict: Dict = {
            "polygon": [[0.0, 0.0], [100.0, 0.0], [100.0, 100.0], [0.0, 100.0]],
            "class_id": 0,
            "class_name": "test",
            "model_class_id": 0,
            "model_class_name": "test",
            "score": 1.0,
        }

        dummy_segmentation_dict = dummy_segmentation.to_dict(reduced=True)

        assert polygon_equal(dummy_segmentation_dict["polygon"], expected_dict["polygon"])
        assert dummy_segmentation_dict["class_id"] == expected_dict["class_id"]
        assert dummy_segmentation_dict["class_name"] == expected_dict["class_name"]
        assert dummy_segmentation_dict["model_class_id"] == expected_dict["model_class_id"]
        assert dummy_segmentation_dict["model_class_name"] == expected_dict["model_class_name"]
        assert isclose(
            dummy_segmentation_dict["score"],
            expected_dict["score"],
            abs_tol=float_equality_precision,
        )

    def test_to_dict_raw_reduced(self) -> None:
        dummy_segmentation: Segmentation = self.__create_dummy_segmentation()
        expected_dict: Dict = {
            "polygon": [[0.0, 0.0], [100.0, 0.0], [100.0, 100.0], [0.0, 100.0]],
            "class_id": 0,
            "class_name": "test",
            "model_class_id": 0,
            "model_class_name": "test",
            "score": 1,
        }

        dummy_segmentation_dict = dummy_segmentation.to_dict(raw_type=True, reduced=True)

        assert polygon_equal(dummy_segmentation_dict["polygon"], expected_dict["polygon"])
        assert dummy_segmentation_dict["class_id"] == expected_dict["class_id"]
        assert dummy_segmentation_dict["class_name"] == expected_dict["class_name"]
        assert dummy_segmentation_dict["model_class_id"] == expected_dict["model_class_id"]
        assert dummy_segmentation_dict["model_class_name"] == expected_dict["model_class_name"]
        assert isclose(
            dummy_segmentation_dict["score"],
            expected_dict["score"],
            abs_tol=float_equality_precision,
        )

    def test_from_dict(self) -> None:
        segmentation_dict: Dict = {
            "polygon": [(0, 0), (100, 0), (100, 100), (0, 100)],
            "class_identifier": {"class_id": 0, "class_name": "test"},
            "model_class_identifier": {"class_id": 0, "class_name": "test"},
            "score": 1,
            "content": "",
            "difficult": False,
            "occluded": False,
            "background": False,
        }
        segmentation: Segmentation = Segmentation.from_dict(segmentation_dict)

        assert segmentation == self.__create_dummy_segmentation()

    def test_from_dict_reduced(self) -> None:
        segmentation_dict: Dict = {
            "polygon": [(0, 0), (100, 0), (100, 100), (0, 100)],
            "class_id": 0,
            "class_name": "test",
            "model_class_id": 0,
            "model_class_name": "test",
            "score": 1,
        }
        segmentation: Segmentation = Segmentation.from_dict(segmentation_dict, reduced=True)

        assert segmentation == self.__create_dummy_segmentation()

    def test_to_json(self) -> None:
        dummy_segmentation: Segmentation = self.__create_dummy_segmentation()
        segmentation_json: Any = dummy_segmentation.to_json()
        expected_json = {
            "polygon": [[0.0, 0.0], [100.0, 0.0], [100.0, 100.0], [0.0, 100.0]],
            "class_identifier": {"class_id": 0, "class_name": "test"},
            "model_class_identifier": {"class_id": 0, "class_name": "test"},
            "score": 1.0,
            "content": "",
            "difficult": False,
            "occluded": False,
            "background": False,
        }
        print(type(segmentation_json))
        assert segmentation_json == expected_json

    def test_to_to_rect_polygon(self) -> None:
        dummy_segmentation: Segmentation = Segmentation(
            polygon=[
                (100, 100),
                (150, 113),
                (150, 150),
                (144, 188),
                (100, 200),
                (30, 214),
                (50, 150),
            ],
            class_identifier=ClassIdentifier(class_id=0, class_name="test"),
            score=1.0,
        )

        polygon = [
            (100, 100),
            (150, 113),
            (150, 150),
            (144, 188),
            (100, 200),
            (30, 214),
            (50, 150),
        ]

        rect_polygon = Segmentation.polygon_to_rect_polygon(polygon=polygon)

        polygon_equal(
            rect_polygon,
            [
                [62.19559, 90.170845],
                [162.64519, 116.287735],
                [130.44962, 240.11688],
                [30.00002, 213.99998],
            ],
        )

    def test_to_str(self) -> None:
        dummy_bounding_segmentation: Segmentation = self.__create_dummy_segmentation()

        assert str(dummy_bounding_segmentation) == (
            "Segmentation("
            'class_identifier="0_test", '
            'model_class_identifier="0_test", '
            "polygon=[[0.0, 0.0], [100.0, 0.0], [100.0, 100.0], [0.0, 100.0]], "
            "score=1.0, "
            "difficult=False, "
            "occluded=False, "
            "background=False, "
            'content="", '
            "meta_attributes={}"
            ")"
        )

    def test_to_repr(self) -> None:
        dummy_bounding_segmentation: Segmentation = self.__create_dummy_segmentation()

        assert f"{repr(dummy_bounding_segmentation)}" == (
            "Segmentation("
            'class_identifier=ClassIdentifier(class_id=0, class_name="test"), '
            'model_class_identifier=ClassIdentifier(class_id=0, class_name="test"), '
            "polygon=[[0.0, 0.0], [100.0, 0.0], [100.0, 100.0], [0.0, 100.0]], "
            "score=1.0, "
            "difficult=False, "
            "occluded=False, "
            "background=False, "
            'content="", '
            "meta_attributes={}"
            ")"
        )

        code = (
            "from mlcvzoo_base.api.data.segmentation import Segmentation;"
            "from mlcvzoo_base.api.data.bounding_box import BoundingBox;"
            "from mlcvzoo_base.api.data.box import Box;"
            "from mlcvzoo_base.api.data.class_identifier import ClassIdentifier;"
            f"s = {repr(dummy_bounding_segmentation)};"
        )

        TestTemplate.execute_code_in_process(code=code)

    def test_segmentation_equal(self) -> None:
        s0 = Segmentation(
            polygon=[(0, 0), (100, 0), (100, 100), (0, 100)],
            class_identifier=ClassIdentifier(
                class_id=0,
                class_name="test",
            ),
            score=0.7,
            difficult=False,
            occluded=False,
            background=False,
            content="",
        )

        s1 = Segmentation(
            polygon=[(0, 0), (100, 0), (100, 100), (0, 100)],
            class_identifier=ClassIdentifier(
                class_id=0,
                class_name="test",
            ),
            score=0.7,
            difficult=False,
            occluded=False,
            background=False,
            content="",
        )

        s2 = Segmentation(
            polygon=[(0, 1), (100, 0), (100, 100), (0, 100)],
            class_identifier=ClassIdentifier(
                class_id=0,
                class_name="test",
            ),
            score=0.7,
            difficult=False,
            occluded=False,
            background=False,
            content="",
        )

        assert s0 == s1
        assert s0 != s2


if __name__ == "__main__":
    main()
