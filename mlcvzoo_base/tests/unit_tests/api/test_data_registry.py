# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

import logging
from unittest import main

from mlcvzoo_base.api.data.bounding_box import BoundingBox
from mlcvzoo_base.api.data.box import Box
from mlcvzoo_base.api.data.class_identifier import ClassIdentifier
from mlcvzoo_base.api.data.classification import Classification
from mlcvzoo_base.api.data.data_registry import DataRegistry
from mlcvzoo_base.api.data.segmentation import Segmentation
from mlcvzoo_base.tests.unit_tests.test_template import TestTemplate

logger = logging.getLogger(__name__)


class TestDataRegistry(TestTemplate):
    @staticmethod
    def __create_dummy_classification__() -> Classification:
        return Classification(
            class_identifier=ClassIdentifier(class_id=0, class_name="test"),
            score=1,
            model_class_identifier=ClassIdentifier(class_id=0, class_name="test"),
        )

    @staticmethod
    def __create_dummy_bbox__() -> BoundingBox:
        return BoundingBox(
            box=Box(xmin=0.0, ymin=0.0, xmax=100.0, ymax=100.0, angle=0.0),
            class_identifier=ClassIdentifier(
                class_id=0,
                class_name="test",
            ),
            score=1.0,
            difficult=False,
            occluded=False,
            background=False,
            content="",
            model_class_identifier=ClassIdentifier(
                class_id=0,
                class_name="test",
            ),
        )

    @staticmethod
    def __create_dummy_segmentation() -> Segmentation:
        return Segmentation(
            polygon=[[0.0, 0.0], [100.0, 0.0], [100.0, 100.0], [0.0, 100.0]],
            class_identifier=ClassIdentifier(class_id=0, class_name="test"),
            model_class_identifier=ClassIdentifier(class_id=0, class_name="test"),
            score=1.0,
            difficult=False,
            occluded=False,
            background=False,
            content="",
        )

    def test_data_registry(self) -> None:
        predictions = [
            self.__create_dummy_classification__(),
            self.__create_dummy_bbox__(),
            self.__create_dummy_classification__(),
        ]

        data_registry = DataRegistry()

        encoded_dicts = []

        for prediction in predictions:
            encoded_dicts.append(data_registry.encode(prediction=prediction))

        decoded_predictions = []
        for encoded_dict in encoded_dicts:
            decoded_predictions.append(data_registry.decode(data=encoded_dict))

        assert decoded_predictions == predictions


if __name__ == "__main__":
    main()
