# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

import inspect
import logging
import os
import shlex
import subprocess
import sys
from pathlib import Path
from threading import Thread
from typing import IO, Any, Dict
from unittest import TestCase, main

import mlcvzoo_base
from mlcvzoo_base.configuration.replacement_config import ReplacementConfig

logger = logging.getLogger(__name__)


class TestTemplate(TestCase):
    def setUp(self) -> None:
        self.this_dir = Path(os.path.dirname(os.path.abspath(__file__))).resolve()

        setup_path = self.this_dir
        while setup_path.exists() and not setup_path.name == mlcvzoo_base.__name__:
            if setup_path == setup_path.parent:
                raise RuntimeError("Could not find setup_path!")
            else:
                setup_path = setup_path.parent
        # One more to be above the target directory
        setup_path = setup_path.parent

        self.project_root = str(setup_path)
        self.code_root = str(setup_path)

        self.string_replacement_map = {
            ReplacementConfig.PROJECT_ROOT_DIR_KEY: self.project_root,
        }
        logger.debug(
            "Setup finished: \n"
            " - this_dir: %s\n"
            " - project_root: %s\n"
            " - code_root: %s\n"
            % (
                self.this_dir,
                self.project_root,
                self.code_root,
            )
        )
        os.chdir(self.code_root)
        logger.info("Changed working directory to: '%s'", self.code_root)

    def _gen_replacement_config(self) -> str:
        path = Path("%s/test_output/" % self.project_root)
        # project_root should point to an already existing location, so do not create parents here
        path.mkdir(exist_ok=True, parents=False)
        path = path / "replacement_config.yaml"
        with open(path, "w", encoding="utf-8") as file:
            file.write("PROJECT_ROOT_DIR: %s\n" % self.project_root)
            return str(path)

    @staticmethod
    def __pipe_to_log(pipe: IO[bytes], level: int) -> None:
        with pipe:
            for line in iter(pipe.readline, b""):  # b'\n'-separated lines
                logger.log(level, "Subprocess: %r", line.decode("utf-8").strip())

    @staticmethod
    def execute_code_in_process(code: str) -> None:
        command = f"{sys.executable} -c '{code}'"

        subproc_env = dict(os.environ.copy())
        subproc_env["LOGLEVEL"] = "INFO"

        logger.debug("Run command: %s", command)
        result = subprocess.Popen(
            shlex.split(command),
            env=subproc_env,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            text=False,
        )
        Thread(
            target=TestTemplate.__pipe_to_log, args=[result.stdout, logging.INFO], daemon=True
        ).start()
        result.wait()

        if result.returncode:
            logger.error("Command '%s' returned with exit code %i", command, result.returncode)
            raise RuntimeError(
                f"Test exited with exitcode != 0, " f"exitcode: {result.returncode}"
            )


if __name__ == "__main__":
    main()
