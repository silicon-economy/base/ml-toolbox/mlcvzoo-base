# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

import logging
import os
from unittest import main

from mlcvzoo_base.api.data.annotation import BaseAnnotation
from mlcvzoo_base.api.data.bounding_box import BoundingBox
from mlcvzoo_base.api.data.box import Box
from mlcvzoo_base.api.data.class_identifier import ClassIdentifier
from mlcvzoo_base.api.data.segmentation import Segmentation
from mlcvzoo_base.configuration.structs import CSVFileNameIdentifier
from mlcvzoo_base.data_preparation.annotation_handler import AnnotationHandler
from mlcvzoo_base.data_preparation.annotation_writer.csv_annotation_writer import (
    CSVAnnotationWriter,
)
from mlcvzoo_base.data_preparation.structs import CSVOutputStringFormats
from mlcvzoo_base.tests.unit_tests.test_template import TestTemplate
from mlcvzoo_base.utils.common_utils import get_current_timestamp

logger = logging.getLogger(__name__)


class TestAnnotationHandler(TestTemplate):
    def test_parse_from_coco(self) -> None:
        yaml_config_path = os.path.join(
            self.project_root,
            "test_data/test_AnnotationHandler",
            "annotation-handler_coco_test.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )

        # TODO: add detailed check for parsed annotations
        annotations = annotation_handler.parse_annotations_from_coco()

    def test_parse_from_coco_all_attributes(self) -> None:
        yaml_config_path = os.path.join(
            self.project_root,
            "test_data/test_AnnotationHandler",
            "annotation-handler_coco_all_attributes_test.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )

        annotations = annotation_handler.parse_annotations_from_coco()

        expected_annotations = [
            BaseAnnotation(
                image_path=os.path.join("test_data/images/dummy_task/cars.jpg"),
                annotation_path=os.path.join(
                    "test_data/annotations/coco/coco_all_attributes.json"
                ),
                image_shape=(500, 375),
                classifications=[],
                bounding_boxes=[
                    BoundingBox(
                        class_identifier=ClassIdentifier(class_id=2, class_name="car"),
                        model_class_identifier=ClassIdentifier(class_id=2, class_name="car"),
                        box=Box(xmin=28.33, ymin=9.28, xmax=130.62, ymax=78.5, angle=0.0),
                        score=1.0,
                        difficult=False,
                        occluded=False,
                        background=False,
                        content="car",
                        meta_attributes={},
                    ),
                    BoundingBox(
                        class_identifier=ClassIdentifier(class_id=2, class_name="car"),
                        model_class_identifier=ClassIdentifier(class_id=2, class_name="car"),
                        box=Box(
                            xmin=32.73, ymin=146.4, xmax=129.29999999999998, ymax=211.22, angle=0.0
                        ),
                        score=1.0,
                        difficult=False,
                        occluded=False,
                        background=True,
                        content="",
                        meta_attributes={},
                    ),
                    BoundingBox(
                        class_identifier=ClassIdentifier(class_id=2, class_name="car"),
                        model_class_identifier=ClassIdentifier(class_id=2, class_name="car"),
                        box=Box(xmin=34.5, ymin=246.05, xmax=128.41, ymax=325.42, angle=0.0),
                        score=1.0,
                        difficult=True,
                        occluded=False,
                        background=False,
                        content="",
                        meta_attributes={},
                    ),
                    BoundingBox(
                        class_identifier=ClassIdentifier(class_id=2, class_name="car"),
                        model_class_identifier=ClassIdentifier(class_id=2, class_name="car"),
                        box=Box(xmin=30.53, ymin=357.16, xmax=125.77, ymax=433.88, angle=0.0),
                        score=1.0,
                        difficult=False,
                        occluded=False,
                        background=False,
                        content="",
                        meta_attributes={},
                    ),
                    BoundingBox(
                        class_identifier=ClassIdentifier(class_id=2, class_name="car"),
                        model_class_identifier=ClassIdentifier(class_id=2, class_name="car"),
                        box=Box(
                            xmin=199.4, ymin=48.08, xmax=321.54, ymax=145.07999999999998, angle=0.0
                        ),
                        score=1.0,
                        difficult=False,
                        occluded=True,
                        background=True,
                        content="",
                        meta_attributes={},
                    ),
                ],
                segmentations=[],
                ocr_perception=None,
                image_dir=os.path.join("test_data/images/dummy_task"),
                annotation_dir=os.path.join("test_data/annotations/coco"),
                replacement_string=os.path.join("test_data/annotations/coco"),
            ),
            BaseAnnotation(
                image_path=os.path.join("test_data/images/dummy_task/empty.jpg"),
                annotation_path=os.path.join(
                    "test_data/annotations/coco/coco_all_attributes.json"
                ),
                image_shape=(500, 375),
                classifications=[],
                bounding_boxes=[],
                segmentations=[],
                ocr_perception=None,
                image_dir=os.path.join("test_data/images/dummy_task"),
                annotation_dir=os.path.join("test_data/annotations/coco"),
                replacement_string=os.path.join("test_data/annotations/coco"),
            ),
            BaseAnnotation(
                image_path=os.path.join(
                    self.project_root, "test_data/images/dummy_task/person.jpg"
                ),
                annotation_path=os.path.join(
                    self.project_root, "test_data/annotations/coco/coco_all_attributes.json"
                ),
                image_shape=(500, 375),
                classifications=[],
                bounding_boxes=[
                    BoundingBox(
                        class_identifier=ClassIdentifier(class_id=0, class_name="person"),
                        model_class_identifier=ClassIdentifier(class_id=0, class_name="person"),
                        box=Box(xmin=28.33, ymin=9.28, xmax=130.62, ymax=78.5, angle=0.0),
                        score=1.0,
                        difficult=False,
                        occluded=False,
                        background=False,
                        content="car",
                        meta_attributes={"other": "test"},
                    )
                ],
                segmentations=[
                    Segmentation(
                        class_identifier=ClassIdentifier(class_id=2, class_name="car"),
                        model_class_identifier=ClassIdentifier(class_id=2, class_name="car"),
                        polygon=[
                            [115.31, 122.73],
                            [230.8, 136.51],
                            [232.77, 220.5],
                            [126.47, 244.78],
                            [95.63, 175.88],
                        ],
                        score=1.0,
                        difficult=False,
                        occluded=False,
                        background=False,
                        content="",
                        meta_attributes={},
                    )
                ],
                ocr_perception=None,
                image_dir=os.path.join(self.project_root, "test_data/images/dummy_task"),
                annotation_dir=os.path.join(self.project_root, "test_data/annotations/coco"),
                replacement_string=os.path.join(self.project_root, "test_data/annotations/coco"),
            ),
            BaseAnnotation(
                image_path=os.path.join("test_data/images/dummy_task/truck.jpg"),
                annotation_path=os.path.join(
                    "test_data/annotations/coco/coco_all_attributes.json"
                ),
                image_shape=(500, 375),
                classifications=[],
                bounding_boxes=[],
                segmentations=[],
                ocr_perception=None,
                image_dir=os.path.join("test_data/images/dummy_task"),
                annotation_dir=os.path.join("test_data/annotations/coco"),
                replacement_string=os.path.join("test_data/annotations/coco"),
            ),
        ]

        assert expected_annotations == annotations

    def test_parse_from_coco_all_attributes_no_difficult(self) -> None:
        yaml_config_path = os.path.join(
            self.project_root,
            "test_data/test_AnnotationHandler",
            "annotation-handler_coco_all_attributes_test.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )
        annotation_handler.configuration.coco_input_data[0].use_difficult = False

        annotations = annotation_handler.parse_annotations_from_coco()

        assert len(annotations) == 4
        assert len(annotations[0].bounding_boxes) == 4
        assert len(annotations[1].bounding_boxes) == 0
        assert len(annotations[2].bounding_boxes) == 1
        assert len(annotations[3].bounding_boxes) == 0

        assert annotations[0].bounding_boxes[0].difficult is False
        assert annotations[0].bounding_boxes[0].occluded is False
        assert annotations[0].bounding_boxes[0].background is False
        assert annotations[0].bounding_boxes[0].content == "car"

        assert annotations[0].bounding_boxes[1].difficult is False
        assert annotations[0].bounding_boxes[1].occluded is False
        assert annotations[0].bounding_boxes[1].background is True
        assert annotations[0].bounding_boxes[1].content is ""

        assert annotations[0].bounding_boxes[2].difficult is False
        assert annotations[0].bounding_boxes[2].occluded is False
        assert annotations[0].bounding_boxes[2].background is False
        assert annotations[0].bounding_boxes[2].content is ""

        assert annotations[0].bounding_boxes[3].difficult is False
        assert annotations[0].bounding_boxes[3].occluded is True
        assert annotations[0].bounding_boxes[3].background is True
        assert annotations[0].bounding_boxes[3].content is ""

    def test_parse_from_coco_all_attributes_no_occluded(self) -> None:
        yaml_config_path = os.path.join(
            self.project_root,
            "test_data/test_AnnotationHandler",
            "annotation-handler_coco_all_attributes_test.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )
        annotation_handler.configuration.coco_input_data[0].use_occluded = False

        annotations = annotation_handler.parse_annotations_from_coco()

        assert len(annotations) == 4
        assert len(annotations[0].bounding_boxes) == 4
        assert len(annotations[1].bounding_boxes) == 0
        assert len(annotations[2].bounding_boxes) == 1
        assert len(annotations[3].bounding_boxes) == 0

        assert annotations[0].bounding_boxes[0].difficult is False
        assert annotations[0].bounding_boxes[0].occluded is False
        assert annotations[0].bounding_boxes[0].background is False
        assert annotations[0].bounding_boxes[0].content == "car"

        assert annotations[0].bounding_boxes[1].difficult is False
        assert annotations[0].bounding_boxes[1].occluded is False
        assert annotations[0].bounding_boxes[1].background is True
        assert annotations[0].bounding_boxes[1].content is ""

        assert annotations[0].bounding_boxes[2].difficult is True
        assert annotations[0].bounding_boxes[2].occluded is False
        assert annotations[0].bounding_boxes[2].background is False
        assert annotations[0].bounding_boxes[2].content is ""

        assert annotations[0].bounding_boxes[3].difficult is False
        assert annotations[0].bounding_boxes[3].occluded is False
        assert annotations[0].bounding_boxes[3].background is False
        assert annotations[0].bounding_boxes[3].content is ""

    def test_parse_from_coco_all_attributes_no_background(self) -> None:
        yaml_config_path = os.path.join(
            self.project_root,
            "test_data/test_AnnotationHandler",
            "annotation-handler_coco_all_attributes_test.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )
        annotation_handler.configuration.coco_input_data[0].use_background = False

        annotations = annotation_handler.parse_annotations_from_coco()

        assert len(annotations) == 4
        assert len(annotations[0].bounding_boxes) == 3
        assert len(annotations[1].bounding_boxes) == 0
        assert len(annotations[2].bounding_boxes) == 1
        assert len(annotations[3].bounding_boxes) == 0

        assert annotations[0].bounding_boxes[0].difficult is False
        assert annotations[0].bounding_boxes[0].occluded is False
        assert annotations[0].bounding_boxes[0].background is False
        assert annotations[0].bounding_boxes[0].content == "car"

        assert annotations[0].bounding_boxes[1].difficult is True
        assert annotations[0].bounding_boxes[1].occluded is False
        assert annotations[0].bounding_boxes[1].background is False
        assert annotations[0].bounding_boxes[1].content is ""

        assert annotations[0].bounding_boxes[2].difficult is False
        assert annotations[0].bounding_boxes[2].occluded is False
        assert annotations[0].bounding_boxes[2].background is False
        assert annotations[0].bounding_boxes[2].content is ""

    def test_parse_from_alternate_coco(self) -> None:
        yaml_config_path = os.path.join(
            self.project_root,
            "test_data/test_AnnotationHandler",
            "annotation-handler_alternate_coco_test.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )

        annotations = annotation_handler.parse_annotations_from_coco()

        assert annotations[0].bounding_boxes[0].difficult is False
        assert annotations[0].bounding_boxes[0].occluded is False
        assert annotations[0].bounding_boxes[0].content == ""

    def test_parse_from_unsupported_coco_segmentation(self) -> None:
        yaml_config_path = os.path.join(
            self.project_root,
            "test_data/test_AnnotationHandler",
            "annotation-handler_coco_segmentation_unsupported.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )

        annotations = annotation_handler.parse_annotations_from_coco()

        # the unsupported segmentation should be skipped and the correct one should be parsed
        assert len(annotations[0].segmentations) == 1

    def test_parse_from_coco_multiple_sources(self) -> None:
        logger.info(
            "############################################################\n"
            "# TEST parsing from multiple coco json:\n"
            "#      test_parse_from_coco_multiple_sources(self)\n"
            "############################################################"
        )

        yaml_config_path = os.path.join(
            self.project_root,
            "test_data/test_AnnotationHandler",
            "annotation-handler_coco_test_multiple_sources.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )

        # TODO: add detailed check for parsed annotations
        annotations = annotation_handler.parse_annotations_from_coco()

        annotation_handler.generate_csv(
            annotations=annotations, output_string_format=CSVOutputStringFormats.BASE
        )

        timestamp_string = get_current_timestamp(
            annotation_handler.configuration.write_output.csv_annotation.timestamp_format
        )

        file_extension = ".csv"

        csv_base_file_name = CSVAnnotationWriter.get_csv_base_file_name(
            timestamp_string=timestamp_string,
            csv_annotation=annotation_handler.configuration.write_output.csv_annotation,
        )

        csv_file_path = CSVAnnotationWriter.generate_csv_path(
            csv_base_file_name=csv_base_file_name,
            file_extension=file_extension,
            file_identifier=CSVFileNameIdentifier.VALIDATION,
            csv_annotation=annotation_handler.configuration.write_output.csv_annotation,
        )

        annotations = annotation_handler.parse_annotations_from_csv(csv_file_path=csv_file_path)

        for index, annotation in enumerate(annotations):
            if not os.path.isfile(annotation.image_path):
                raise ValueError(
                    f"annotation.image_path '{annotation.image_path}' "
                    f"at index {index} parsed from '{csv_file_path}' does not exist"
                )


if __name__ == "__main__":
    main()
