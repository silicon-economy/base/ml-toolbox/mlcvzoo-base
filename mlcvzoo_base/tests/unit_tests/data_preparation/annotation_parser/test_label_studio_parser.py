# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

import logging
import os
from unittest import main

from mlcvzoo_base.api.data.annotation import BaseAnnotation
from mlcvzoo_base.api.data.bounding_box import BoundingBox
from mlcvzoo_base.api.data.box import Box
from mlcvzoo_base.api.data.class_identifier import ClassIdentifier
from mlcvzoo_base.api.data.classification import Classification
from mlcvzoo_base.api.data.segmentation import Segmentation
from mlcvzoo_base.data_preparation.annotation_handler import AnnotationHandler
from mlcvzoo_base.tests.unit_tests.test_template import TestTemplate

logger = logging.getLogger(__name__)


class TestLabelStudioAnnotationParser(TestTemplate):
    def test_parse_from_label_studio_image_od(self) -> None:
        yaml_config_path = os.path.join(
            self.project_root,
            "test_data/test_AnnotationHandler",
            "annotation-handler_label-studio_test.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )

        annotations = annotation_handler.parse_annotations_from_label_studio()

        annotations = sorted(annotations, key=lambda x: x.image_path)

        # TODO: add detailed check for parsed annotations
        assert len(annotations) == 3
        assert annotations[0].image_path == os.path.join(
            self.project_root, "test_data/images/dummy_task/cars.jpg"
        )
        assert annotations[1].image_path == os.path.join(
            self.project_root, "test_data/images/dummy_task/person.jpg"
        )
        assert annotations[2].image_path == os.path.join(
            self.project_root, "test_data/images/dummy_task/truck.jpg"
        )

    def test_parse_from_label_studio_image_ocr(self) -> None:
        yaml_config_path = os.path.join(
            self.project_root,
            "test_data/test_AnnotationHandler/annotation-handler_label-studio_ocr_test.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )

        annotations = annotation_handler.parse_annotations_from_label_studio()

        annotations = sorted(annotations, key=lambda x: x.image_path)

        expected_annotations = [
            BaseAnnotation(
                image_path=os.path.join(self.project_root, "test_data/images/dummy_task/cars.jpg"),
                annotation_path=os.path.join(
                    self.project_root,
                    "test_data/annotations/label_studio/label_studio_s3_ocr/dummy_task/cars",
                ),
                image_shape=(500, 375),
                classifications=[],
                bounding_boxes=[
                    BoundingBox(
                        class_identifier=ClassIdentifier(class_id=0, class_name="ilu-code"),
                        model_class_identifier=ClassIdentifier(class_id=0, class_name="ilu-code"),
                        box=Box(
                            xmin=73.09941520467837,
                            ymin=355.7504873294347,
                            xmax=255.3606237816764,
                            ymax=425.9259259259259,
                            angle=0.0,
                        ),
                        score=1.0,
                        difficult=False,
                        occluded=False,
                        background=False,
                        content="hello-ilu-code",
                        meta_attributes={},
                    )
                ],
                segmentations=[
                    Segmentation(
                        class_identifier=ClassIdentifier(class_id=1, class_name="license-plate"),
                        model_class_identifier=ClassIdentifier(
                            class_id=1, class_name="license-plate"
                        ),
                        polygon=[
                            [87.71929824561401, 143.27485380116957],
                            [189.08382066276803, 150.09746588693957],
                            [174.46393762183234, 221.2475633528265],
                            [85.7699805068226, 217.34892787524365],
                        ],
                        score=1.0,
                        difficult=False,
                        occluded=False,
                        background=False,
                        content="hello-license-plate",
                        meta_attributes={},
                    )
                ],
                ocr_perception=None,
                image_dir=os.path.join(self.project_root, "test_data/images/dummy_task"),
                annotation_dir=os.path.join(
                    self.project_root,
                    "test_data/annotations/label_studio/label_studio_s3_ocr/dummy_task",
                ),
                replacement_string=os.path.join(
                    self.project_root,
                    "test_data/annotations/label_studio/label_studio_s3_ocr/dummy_task",
                ),
            ),
            BaseAnnotation(
                image_path=os.path.join(
                    self.project_root, "test_data/images/dummy_task/empty.jpg"
                ),
                annotation_path=os.path.join(
                    self.project_root,
                    "test_data/annotations/label_studio/label_studio_s3_ocr/dummy_task/empty",
                ),
                image_shape=(500, 375),
                classifications=[],
                bounding_boxes=[
                    BoundingBox(
                        class_identifier=ClassIdentifier(class_id=0, class_name="ilu-code"),
                        model_class_identifier=ClassIdentifier(class_id=0, class_name="ilu-code"),
                        box=Box(
                            xmin=83.8206627680312,
                            ymin=160.8187134502924,
                            xmax=251.4619883040936,
                            ymax=261.20857699805066,
                            angle=0.0,
                        ),
                        score=1.0,
                        difficult=False,
                        occluded=False,
                        background=False,
                        content="",
                        meta_attributes={},
                    )
                ],
                segmentations=[],
                ocr_perception=None,
                image_dir=os.path.join(self.project_root, "test_data/images/dummy_task"),
                annotation_dir=os.path.join(
                    self.project_root,
                    "test_data/annotations/label_studio/label_studio_s3_ocr/dummy_task",
                ),
                replacement_string=os.path.join(
                    self.project_root,
                    "test_data/annotations/label_studio/label_studio_s3_ocr/dummy_task",
                ),
            ),
            BaseAnnotation(
                image_path=os.path.join(
                    self.project_root, "test_data/images/dummy_task/person.jpg"
                ),
                annotation_path=os.path.join(
                    self.project_root,
                    "test_data/annotations/label_studio/label_studio_s3_ocr/dummy_task/person",
                ),
                image_shape=(500, 375),
                classifications=[],
                bounding_boxes=[
                    BoundingBox(
                        class_identifier=ClassIdentifier(class_id=0, class_name="ilu-code"),
                        model_class_identifier=ClassIdentifier(class_id=0, class_name="ilu-code"),
                        box=Box(
                            xmin=88.69395711500974,
                            ymin=193.9571150097466,
                            xmax=241.71539961013647,
                            ymax=322.61208576998047,
                            angle=0.0,
                        ),
                        score=1.0,
                        difficult=False,
                        occluded=False,
                        background=False,
                        content="test",
                        meta_attributes={},
                    )
                ],
                segmentations=[],
                ocr_perception=None,
                image_dir=os.path.join(self.project_root, "test_data/images/dummy_task"),
                annotation_dir=os.path.join(
                    self.project_root,
                    "test_data/annotations/label_studio/label_studio_s3_ocr/dummy_task",
                ),
                replacement_string=os.path.join(
                    self.project_root,
                    "test_data/annotations/label_studio/label_studio_s3_ocr/dummy_task",
                ),
            ),
            BaseAnnotation(
                image_path=os.path.join(
                    self.project_root, "test_data/images/dummy_task/truck.jpg"
                ),
                annotation_path=os.path.join(
                    self.project_root,
                    "test_data/annotations/label_studio/label_studio_s3_ocr/dummy_task/truck",
                ),
                image_shape=(500, 375),
                classifications=[],
                bounding_boxes=[
                    BoundingBox(
                        class_identifier=ClassIdentifier(class_id=1, class_name="license-plate"),
                        model_class_identifier=ClassIdentifier(
                            class_id=1, class_name="license-plate"
                        ),
                        box=Box(
                            xmin=66.27680311890838,
                            ymin=130.60428849902533,
                            xmax=221.2475633528265,
                            ymax=209.55165692007796,
                            angle=0.0,
                        ),
                        score=1.0,
                        difficult=False,
                        occluded=False,
                        background=False,
                        content="",
                        meta_attributes={},
                    ),
                    BoundingBox(
                        class_identifier=ClassIdentifier(class_id=1, class_name="license-plate"),
                        model_class_identifier=ClassIdentifier(
                            class_id=1, class_name="license-plate"
                        ),
                        box=Box(
                            xmin=120.46569442749023,
                            ymin=279.5678405761719,
                            xmax=228.37768936157227,
                            ymax=409.0158996582031,
                            angle=69.3818130493164,
                        ),
                        score=1.0,
                        difficult=False,
                        occluded=False,
                        background=False,
                        content="test",
                        meta_attributes={},
                    ),
                ],
                segmentations=[],
                ocr_perception=None,
                image_dir=os.path.join(self.project_root, "test_data/images/dummy_task"),
                annotation_dir=os.path.join(
                    self.project_root,
                    "test_data/annotations/label_studio/label_studio_s3_ocr/dummy_task",
                ),
                replacement_string=os.path.join(
                    self.project_root,
                    "test_data/annotations/label_studio/label_studio_s3_ocr/dummy_task",
                ),
            ),
        ]

        assert annotations == expected_annotations

    def test_parse_from_label_studio_single_image_segmentation(self) -> None:
        yaml_config_path = os.path.join(
            self.project_root,
            "test_data/test_AnnotationHandler",
            "annotation-handler_label-studio-single_segmentation_test.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )

        annotations = annotation_handler.parse_annotations_from_label_studio_single()

        annotations = sorted(annotations, key=lambda x: x.image_path)

        expected_annotations = [
            BaseAnnotation(
                image_path=os.path.join(self.project_root, "test_data/images/dummy_task/cars.jpg"),
                annotation_path=os.path.join(
                    self.project_root,
                    "test_data/annotations/label_studio/label_studio_dummy_task_segmentation.json",
                ),
                image_shape=(500, 375),
                classifications=[],
                bounding_boxes=[],
                segmentations=[
                    Segmentation(
                        class_identifier=ClassIdentifier(class_id=2, class_name="car"),
                        model_class_identifier=ClassIdentifier(class_id=2, class_name="car"),
                        polygon=[
                            [201.62297128589265, 164.1697877652934],
                            [124.84394506866417, 196.6292134831461],
                            [88.01498127340825, 287.14107365792756],
                            [216.60424469413235, 343.94506866416987],
                            [277.15355805243445, 264.0449438202247],
                        ],
                        score=1.0,
                        difficult=False,
                        occluded=False,
                        background=False,
                        content="",
                        meta_attributes={},
                    )
                ],
                ocr_perception=None,
                image_dir=os.path.join(self.project_root, "test_data/images/dummy_task"),
                annotation_dir=os.path.join(
                    self.project_root, "test_data/annotations/label_studio"
                ),
                replacement_string=os.path.join(
                    self.project_root, "test_data/annotations/label_studio"
                ),
            ),
            BaseAnnotation(
                image_path=os.path.join(
                    self.project_root, "test_data/images/dummy_task/empty.jpg"
                ),
                annotation_path=os.path.join(
                    self.project_root,
                    "test_data/annotations/label_studio/label_studio_dummy_task_segmentation.json",
                ),
                image_shape=(500, 375),
                classifications=[],
                bounding_boxes=[],
                segmentations=[],
                ocr_perception=None,
                image_dir=os.path.join(self.project_root, "test_data/images/dummy_task"),
                annotation_dir=os.path.join(
                    self.project_root, "test_data/annotations/label_studio"
                ),
                replacement_string=os.path.join(
                    self.project_root, "test_data/annotations/label_studio"
                ),
            ),
            BaseAnnotation(
                image_path=os.path.join(
                    self.project_root, "test_data/images/dummy_task/person.jpg"
                ),
                annotation_path=os.path.join(
                    self.project_root,
                    "test_data/annotations/label_studio/label_studio_dummy_task_segmentation.json",
                ),
                image_shape=(500, 375),
                classifications=[],
                bounding_boxes=[],
                segmentations=[
                    Segmentation(
                        class_identifier=ClassIdentifier(class_id=0, class_name="person"),
                        model_class_identifier=ClassIdentifier(class_id=0, class_name="person"),
                        polygon=[
                            [129.21348314606743, 199.75031210986265],
                            [236.5792759051186, 164.1697877652934],
                            [287.1410736579276, 304.6192259675406],
                            [247.81523096129837, 396.37952559300874],
                            [77.4032459425718, 373.9076154806492],
                            [63.670411985018724, 224.09488139825217],
                        ],
                        score=1.0,
                        difficult=False,
                        occluded=False,
                        background=False,
                        content="",
                        meta_attributes={},
                    )
                ],
                ocr_perception=None,
                image_dir=os.path.join(self.project_root, "test_data/images/dummy_task"),
                annotation_dir=os.path.join(
                    self.project_root, "test_data/annotations/label_studio"
                ),
                replacement_string=os.path.join(
                    self.project_root, "test_data/annotations/label_studio"
                ),
            ),
            BaseAnnotation(
                image_path=os.path.join(
                    self.project_root, "test_data/images/dummy_task/truck.jpg"
                ),
                annotation_path=os.path.join(
                    self.project_root,
                    "test_data/annotations/label_studio/label_studio_dummy_task_segmentation.json",
                ),
                image_shape=(500, 375),
                classifications=[],
                bounding_boxes=[],
                segmentations=[
                    Segmentation(
                        class_identifier=ClassIdentifier(class_id=1, class_name="truck"),
                        model_class_identifier=ClassIdentifier(class_id=1, class_name="truck"),
                        polygon=[
                            [99.250936329588, 94.25717852684144],
                            [159.80024968789016, 49.31335830212235],
                            [187.26591760299627, 81.77278401997503],
                            [139.82521847690387, 162.29712858926345],
                        ],
                        score=1.0,
                        difficult=False,
                        occluded=False,
                        background=False,
                        content="",
                        meta_attributes={},
                    ),
                    Segmentation(
                        class_identifier=ClassIdentifier(class_id=1, class_name="truck"),
                        model_class_identifier=ClassIdentifier(class_id=1, class_name="truck"),
                        polygon=[
                            [205.36828963795256, 346.4419475655431],
                            [289.63795255930086, 257.1785268414482],
                            [317.10362047440697, 338.3270911360799],
                            [302.1223470661673, 428.83895131086143],
                        ],
                        score=1.0,
                        difficult=False,
                        occluded=False,
                        background=False,
                        content="",
                        meta_attributes={},
                    ),
                ],
                ocr_perception=None,
                image_dir=os.path.join(self.project_root, "test_data/images/dummy_task"),
                annotation_dir=os.path.join(
                    self.project_root, "test_data/annotations/label_studio"
                ),
                replacement_string=os.path.join(
                    self.project_root, "test_data/annotations/label_studio"
                ),
            ),
        ]

        assert annotations == expected_annotations

    def test_parse_from_label_studio_image_ocr_single(self) -> None:
        yaml_config_path = os.path.join(
            self.project_root,
            "test_data/test_AnnotationHandler",
            "annotation-handler_label-studio-single_ocr_test.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )

        annotations = annotation_handler.parse_annotations_from_label_studio_single()

        annotations = sorted(annotations, key=lambda x: x.image_path)

        expected_annotations = [
            BaseAnnotation(
                image_path=os.path.join(self.project_root, "test_data/images/dummy_task/cars.jpg"),
                annotation_path=os.path.join(
                    self.project_root,
                    "test_data/annotations/label_studio/label_studio_dummy_task_ocr.json",
                ),
                image_shape=(500, 375),
                classifications=[],
                bounding_boxes=[
                    BoundingBox(
                        class_identifier=ClassIdentifier(class_id=0, class_name="ilu-code"),
                        model_class_identifier=ClassIdentifier(class_id=0, class_name="ilu-code"),
                        box=Box(
                            xmin=73.09941520467837,
                            ymin=355.7504873294347,
                            xmax=255.3606237816764,
                            ymax=425.9259259259259,
                            angle=0.0,
                        ),
                        score=1.0,
                        difficult=False,
                        occluded=False,
                        background=False,
                        content="hello-ilu-code",
                        meta_attributes={},
                    )
                ],
                segmentations=[
                    Segmentation(
                        class_identifier=ClassIdentifier(class_id=1, class_name="license-plate"),
                        model_class_identifier=ClassIdentifier(
                            class_id=1, class_name="license-plate"
                        ),
                        polygon=[
                            [87.71929824561401, 143.27485380116957],
                            [189.08382066276803, 150.09746588693957],
                            [174.46393762183234, 221.2475633528265],
                            [85.7699805068226, 217.34892787524365],
                        ],
                        score=1.0,
                        difficult=False,
                        occluded=False,
                        background=False,
                        content="hello-license-plate",
                        meta_attributes={},
                    )
                ],
                ocr_perception=None,
                image_dir=os.path.join(self.project_root, "test_data/images/dummy_task"),
                annotation_dir=os.path.join(
                    self.project_root, "test_data/annotations/label_studio"
                ),
                replacement_string=os.path.join(
                    self.project_root, "test_data/annotations/label_studio"
                ),
            ),
            BaseAnnotation(
                image_path=os.path.join(
                    self.project_root, "test_data/images/dummy_task/empty.jpg"
                ),
                annotation_path=os.path.join(
                    self.project_root,
                    "test_data/annotations/label_studio/label_studio_dummy_task_ocr.json",
                ),
                image_shape=(500, 375),
                classifications=[],
                bounding_boxes=[
                    BoundingBox(
                        class_identifier=ClassIdentifier(class_id=0, class_name="ilu-code"),
                        model_class_identifier=ClassIdentifier(class_id=0, class_name="ilu-code"),
                        box=Box(
                            xmin=83.8206627680312,
                            ymin=160.8187134502924,
                            xmax=251.4619883040936,
                            ymax=261.20857699805066,
                            angle=0.0,
                        ),
                        score=1.0,
                        difficult=False,
                        occluded=False,
                        background=False,
                        content="",
                        meta_attributes={},
                    )
                ],
                segmentations=[],
                ocr_perception=None,
                image_dir=os.path.join(self.project_root, "test_data/images/dummy_task"),
                annotation_dir=os.path.join(
                    self.project_root, "test_data/annotations/label_studio"
                ),
                replacement_string=os.path.join(
                    self.project_root, "test_data/annotations/label_studio"
                ),
            ),
            BaseAnnotation(
                image_path=os.path.join(
                    self.project_root, "test_data/images/dummy_task/person.jpg"
                ),
                annotation_path=os.path.join(
                    self.project_root,
                    "test_data/annotations/label_studio/label_studio_dummy_task_ocr.json",
                ),
                image_shape=(500, 375),
                classifications=[],
                bounding_boxes=[
                    BoundingBox(
                        class_identifier=ClassIdentifier(class_id=0, class_name="ilu-code"),
                        model_class_identifier=ClassIdentifier(class_id=0, class_name="ilu-code"),
                        box=Box(
                            xmin=88.69395711500974,
                            ymin=193.9571150097466,
                            xmax=241.71539961013647,
                            ymax=322.61208576998047,
                            angle=0.0,
                        ),
                        score=1.0,
                        difficult=False,
                        occluded=False,
                        background=False,
                        content="test",
                        meta_attributes={},
                    )
                ],
                segmentations=[],
                ocr_perception=None,
                image_dir=os.path.join(self.project_root, "test_data/images/dummy_task"),
                annotation_dir=os.path.join(
                    self.project_root, "test_data/annotations/label_studio"
                ),
                replacement_string=os.path.join(
                    self.project_root, "test_data/annotations/label_studio"
                ),
            ),
            BaseAnnotation(
                image_path=os.path.join(
                    self.project_root, "test_data/images/dummy_task/truck.jpg"
                ),
                annotation_path=os.path.join(
                    self.project_root,
                    "test_data/annotations/label_studio/label_studio_dummy_task_ocr.json",
                ),
                image_shape=(500, 375),
                classifications=[],
                bounding_boxes=[
                    BoundingBox(
                        class_identifier=ClassIdentifier(class_id=1, class_name="license-plate"),
                        model_class_identifier=ClassIdentifier(
                            class_id=1, class_name="license-plate"
                        ),
                        box=Box(
                            xmin=66.27680311890838,
                            ymin=130.60428849902533,
                            xmax=221.2475633528265,
                            ymax=209.55165692007796,
                            angle=0.0,
                        ),
                        score=1.0,
                        difficult=False,
                        occluded=False,
                        background=False,
                        content="",
                        meta_attributes={},
                    ),
                    BoundingBox(
                        class_identifier=ClassIdentifier(class_id=1, class_name="license-plate"),
                        model_class_identifier=ClassIdentifier(
                            class_id=1, class_name="license-plate"
                        ),
                        box=Box(
                            xmin=120.46569442749023,
                            ymin=279.5678405761719,
                            xmax=228.37768936157227,
                            ymax=409.0158996582031,
                            angle=69.3818130493164,
                        ),
                        score=1.0,
                        difficult=False,
                        occluded=False,
                        background=False,
                        content="test",
                        meta_attributes={},
                    ),
                ],
                segmentations=[],
                ocr_perception=None,
                image_dir=os.path.join(self.project_root, "test_data/images/dummy_task"),
                annotation_dir=os.path.join(
                    self.project_root, "test_data/annotations/label_studio"
                ),
                replacement_string=os.path.join(
                    self.project_root, "test_data/annotations/label_studio"
                ),
            ),
        ]

        assert annotations == expected_annotations

    def test_parse_from_label_studio_single(self) -> None:
        yaml_config_path = os.path.join(
            self.project_root,
            "test_data/test_AnnotationHandler/"
            "annotation-handler_label-studio-single_video_test.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )

        annotations = annotation_handler.parse_annotations_from_label_studio_single()

        annotations = sorted(annotations, key=lambda x: x.image_path)

        assert len(annotations) == 50

    def test_parse_label_studio_classifications(self) -> None:
        yaml_config_path = os.path.join(
            self.project_root,
            "test_data/test_AnnotationHandler/"
            "annotation-handler_label-studio_classification_test.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )

        # create new dummy annotations per known annotations
        annotations = annotation_handler.parse_training_annotations()

        annotations = sorted(annotations, key=lambda x: x.image_path)

        expected_annotations = [
            BaseAnnotation(
                image_path=os.path.join(
                    self.project_root,
                    "test_data/images/classification_test_dataset/black/black_image.jpg",
                ),
                annotation_path=os.path.join(
                    self.project_root,
                    "test_data/annotations/label_studio/label_studio_classification/black_image",
                ),
                image_shape=(500, 375),
                classifications=[
                    Classification(
                        class_identifier=ClassIdentifier(class_id=0, class_name="black"),
                        model_class_identifier=ClassIdentifier(class_id=0, class_name="black"),
                        score=1.0,
                    )
                ],
                bounding_boxes=[],
                segmentations=[],
                ocr_perception=None,
                image_dir=os.path.join(
                    self.project_root, "test_data/images/classification_test_dataset/black"
                ),
                annotation_dir=os.path.join(
                    self.project_root,
                    "test_data/annotations/label_studio/label_studio_classification",
                ),
                replacement_string=os.path.join(
                    self.project_root,
                    "test_data/annotations/label_studio/label_studio_classification",
                ),
            ),
            BaseAnnotation(
                image_path=os.path.join(
                    self.project_root,
                    "test_data/images/classification_test_dataset/blue/blue_image.jpg",
                ),
                annotation_path=os.path.join(
                    self.project_root,
                    "test_data/annotations/label_studio/label_studio_classification/blue_image",
                ),
                image_shape=(500, 375),
                classifications=[
                    Classification(
                        class_identifier=ClassIdentifier(class_id=1, class_name="blue"),
                        model_class_identifier=ClassIdentifier(class_id=1, class_name="blue"),
                        score=1.0,
                    )
                ],
                bounding_boxes=[],
                segmentations=[],
                ocr_perception=None,
                image_dir=os.path.join(
                    self.project_root, "test_data/images/classification_test_dataset/blue"
                ),
                annotation_dir=os.path.join(
                    self.project_root,
                    "test_data/annotations/label_studio/label_studio_classification",
                ),
                replacement_string=os.path.join(
                    self.project_root,
                    "test_data/annotations/label_studio/label_studio_classification",
                ),
            ),
            BaseAnnotation(
                image_path=os.path.join(
                    self.project_root,
                    "test_data/images/classification_test_dataset/red/red_image.jpg",
                ),
                annotation_path=os.path.join(
                    self.project_root,
                    "test_data/annotations/label_studio/label_studio_classification/red_image",
                ),
                image_shape=(500, 375),
                classifications=[
                    Classification(
                        class_identifier=ClassIdentifier(class_id=2, class_name="red"),
                        model_class_identifier=ClassIdentifier(class_id=2, class_name="red"),
                        score=1.0,
                    )
                ],
                bounding_boxes=[],
                segmentations=[],
                ocr_perception=None,
                image_dir=os.path.join(
                    self.project_root, "test_data/images/classification_test_dataset/red"
                ),
                annotation_dir=os.path.join(
                    self.project_root,
                    "test_data/annotations/label_studio/label_studio_classification",
                ),
                replacement_string=os.path.join(
                    self.project_root,
                    "test_data/annotations/label_studio/label_studio_classification",
                ),
            ),
        ]

        assert annotations == expected_annotations

    def test_parse_label_studio_classifications_single(self) -> None:
        yaml_config_path = os.path.join(
            self.project_root,
            "test_data/test_AnnotationHandler/"
            "annotation-handler_label-studio-single_classification_test.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )

        # create new dummy annotations per known annotations
        annotations = annotation_handler.parse_training_annotations()

        annotations = sorted(annotations, key=lambda x: x.image_path)

        expected_annotations = [
            BaseAnnotation(
                image_path=os.path.join(
                    self.project_root,
                    "test_data/images/classification_test_dataset/black/black_image.jpg",
                ),
                annotation_path=os.path.join(
                    self.project_root,
                    "test_data/annotations/label_studio/label_studio_classification/black_image",
                ),
                image_shape=(500, 375),
                classifications=[
                    Classification(
                        class_identifier=ClassIdentifier(class_id=0, class_name="black"),
                        model_class_identifier=ClassIdentifier(class_id=0, class_name="black"),
                        score=1.0,
                    )
                ],
                bounding_boxes=[],
                segmentations=[],
                ocr_perception=None,
                image_dir=os.path.join(
                    self.project_root, "test_data/images/classification_test_dataset/black"
                ),
                annotation_dir=os.path.join(
                    self.project_root,
                    "test_data/annotations/label_studio/label_studio_classification",
                ),
                replacement_string=os.path.join(
                    self.project_root,
                    "test_data/annotations/label_studio/label_studio_classification",
                ),
            ),
            BaseAnnotation(
                image_path=os.path.join(
                    self.project_root,
                    "test_data/images/classification_test_dataset/blue/blue_image.jpg",
                ),
                annotation_path=os.path.join(
                    self.project_root,
                    "test_data/annotations/label_studio/label_studio_classification/blue_image",
                ),
                image_shape=(500, 375),
                classifications=[
                    Classification(
                        class_identifier=ClassIdentifier(class_id=1, class_name="blue"),
                        model_class_identifier=ClassIdentifier(class_id=1, class_name="blue"),
                        score=1.0,
                    )
                ],
                bounding_boxes=[],
                segmentations=[],
                ocr_perception=None,
                image_dir=os.path.join(
                    self.project_root, "test_data/images/classification_test_dataset/blue"
                ),
                annotation_dir=os.path.join(
                    self.project_root,
                    "test_data/annotations/label_studio/label_studio_classification",
                ),
                replacement_string=os.path.join(
                    self.project_root,
                    "test_data/annotations/label_studio/label_studio_classification",
                ),
            ),
            BaseAnnotation(
                image_path=os.path.join(
                    self.project_root,
                    "test_data/images/classification_test_dataset/red/red_image.jpg",
                ),
                annotation_path=os.path.join(
                    self.project_root,
                    "test_data/annotations/label_studio/label_studio_classification/red_image",
                ),
                image_shape=(500, 375),
                classifications=[
                    Classification(
                        class_identifier=ClassIdentifier(class_id=2, class_name="red"),
                        model_class_identifier=ClassIdentifier(class_id=2, class_name="red"),
                        score=1.0,
                    )
                ],
                bounding_boxes=[],
                segmentations=[],
                ocr_perception=None,
                image_dir=os.path.join(
                    self.project_root, "test_data/images/classification_test_dataset/red"
                ),
                annotation_dir=os.path.join(
                    self.project_root,
                    "test_data/annotations/label_studio/label_studio_classification",
                ),
                replacement_string=os.path.join(
                    self.project_root,
                    "test_data/annotations/label_studio/label_studio_classification",
                ),
            ),
        ]

        assert annotations == expected_annotations

    def test_parse_from_label_studio_single_from_exported(self) -> None:
        yaml_config_path = os.path.join(
            self.project_root,
            "test_data/test_AnnotationHandler/"
            "annotation-handler_label-studio_from-exported_test.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )

        annotations = annotation_handler.parse_annotations_from_label_studio_single()

        annotations = sorted(annotations, key=lambda x: x.image_path)

        # TODO: add detailed check for parsed annotations
        # TODO: Should a complete image be skipped when the annotations
        #       list in the label-studio annotation-file is emtpy?
        assert len(annotations) == 4
        assert annotations[0].image_path == os.path.join(
            self.project_root, "test_data/images/dummy_task/cars.jpg"
        )
        assert annotations[1].image_path == os.path.join(
            self.project_root, "test_data/images/dummy_task/empty.jpg"
        )
        assert annotations[2].image_path == os.path.join(
            self.project_root, "test_data/images/dummy_task/person.jpg"
        )
        assert annotations[3].image_path == os.path.join(
            self.project_root, "test_data/images/dummy_task/truck.jpg"
        )

    def test_parse_from_label_studio_errors(self) -> None:
        logger.info(
            "############################################################\n"
            "# TEST parsing from label studio json:\n"
            "#      test_parse_from_label_studio(self)\n"
            "############################################################"
        )

        yaml_config_path = os.path.join(
            self.project_root,
            "test_data/test_AnnotationHandler",
            "annotation-handler_label-studio_errors_test.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )

        annotations = annotation_handler.parse_annotations_from_label_studio()

        annotations = sorted(annotations, key=lambda x: x.image_path)

        # TODO: add detailed check for parsed annotations
        assert len(annotations) == 1
        assert annotations[0].image_path == os.path.join(
            self.project_root, "test_data/images/dummy_task/cars.jpg"
        )


if __name__ == "__main__":
    main()
