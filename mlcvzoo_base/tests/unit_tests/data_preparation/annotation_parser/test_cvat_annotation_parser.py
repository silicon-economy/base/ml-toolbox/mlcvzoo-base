# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

import logging
import os
from unittest import main

from mlcvzoo_base.api.data.annotation import BaseAnnotation
from mlcvzoo_base.api.data.bounding_box import BoundingBox
from mlcvzoo_base.api.data.box import Box
from mlcvzoo_base.api.data.class_identifier import ClassIdentifier
from mlcvzoo_base.api.data.classification import Classification
from mlcvzoo_base.api.data.segmentation import Segmentation
from mlcvzoo_base.data_preparation.annotation_handler import AnnotationHandler
from mlcvzoo_base.tests.unit_tests.test_template import TestTemplate

logger = logging.getLogger(__name__)


class TestCVATAnnotationParser(TestTemplate):
    def test_parse_from_cvat(self) -> None:
        logger.info(
            "############################################################\n"
            "# TEST parsing from cvat xml:\n"
            "#      test_parse_from_cvat(self)\n"
            "############################################################"
        )

        yaml_config_path = os.path.join(
            self.project_root,
            "test_data/test_AnnotationHandler",
            "annotation-handler_cvat_test.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )

        # TODO: add detailed check for parsed annotations
        annotations = annotation_handler.parse_annotations_from_cvat()

    def test_parse_from_cvat_all_attributes(self) -> None:
        yaml_config_path = os.path.join(
            self.project_root,
            "test_data/test_AnnotationHandler",
            "annotation-handler_cvat_all_attributes_test.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )

        annotations = annotation_handler.parse_annotations_from_cvat()

        expected_annotations = [
            BaseAnnotation(
                image_path=os.path.join(self.project_root, "test_data/images/dummy_task/cars.jpg"),
                annotation_path=os.path.join(
                    self.project_root, "test_data/annotations/cvat/cvat-all-attributes.xml"
                ),
                image_shape=(500, 375),
                classifications=[],
                bounding_boxes=[
                    BoundingBox(
                        class_identifier=ClassIdentifier(class_id=2, class_name="car"),
                        model_class_identifier=ClassIdentifier(class_id=2, class_name="car"),
                        box=Box(xmin=28.33, ymin=9.28, xmax=130.62, ymax=78.5, angle=0.0),
                        score=1.0,
                        difficult=False,
                        occluded=False,
                        background=False,
                        content="car",
                        meta_attributes={},
                    ),
                    BoundingBox(
                        class_identifier=ClassIdentifier(class_id=2, class_name="car"),
                        model_class_identifier=ClassIdentifier(class_id=2, class_name="car"),
                        box=Box(xmin=32.73, ymin=146.4, xmax=129.3, ymax=211.22, angle=0.0),
                        score=1.0,
                        difficult=False,
                        occluded=False,
                        background=True,
                        content="",
                        meta_attributes={},
                    ),
                    BoundingBox(
                        class_identifier=ClassIdentifier(class_id=2, class_name="car"),
                        model_class_identifier=ClassIdentifier(class_id=2, class_name="car"),
                        box=Box(xmin=34.5, ymin=246.05, xmax=128.41, ymax=325.42, angle=0.0),
                        score=1.0,
                        difficult=True,
                        occluded=False,
                        background=False,
                        content="",
                        meta_attributes={},
                    ),
                    BoundingBox(
                        class_identifier=ClassIdentifier(class_id=2, class_name="car"),
                        model_class_identifier=ClassIdentifier(class_id=2, class_name="car"),
                        box=Box(xmin=30.53, ymin=357.16, xmax=125.77, ymax=433.88, angle=0.0),
                        score=1.0,
                        difficult=False,
                        occluded=False,
                        background=False,
                        content="",
                        meta_attributes={},
                    ),
                    BoundingBox(
                        class_identifier=ClassIdentifier(class_id=2, class_name="car"),
                        model_class_identifier=ClassIdentifier(class_id=2, class_name="car"),
                        box=Box(xmin=199.4, ymin=48.08, xmax=321.54, ymax=145.08, angle=0.0),
                        score=1.0,
                        difficult=False,
                        occluded=True,
                        background=True,
                        content="",
                        meta_attributes={},
                    ),
                ],
                segmentations=[],
                ocr_perception=None,
                image_dir=os.path.join(self.project_root, "test_data/images/dummy_task"),
                annotation_dir=os.path.join(self.project_root, "test_data/images/dummy_task"),
                replacement_string=os.path.join(self.project_root, "test_data/images/dummy_task"),
            ),
            BaseAnnotation(
                image_path=os.path.join(
                    self.project_root, "test_data/images/dummy_task/empty.jpg"
                ),
                annotation_path=os.path.join(
                    self.project_root, "test_data/annotations/cvat/cvat-all-attributes.xml"
                ),
                image_shape=(500, 375),
                classifications=[],
                bounding_boxes=[],
                segmentations=[],
                ocr_perception=None,
                image_dir=os.path.join(self.project_root, "test_data/images/dummy_task"),
                annotation_dir=os.path.join(self.project_root, "test_data/images/dummy_task"),
                replacement_string=os.path.join(self.project_root, "test_data/images/dummy_task"),
            ),
            BaseAnnotation(
                image_path=os.path.join(
                    self.project_root, "test_data/images/dummy_task/person.jpg"
                ),
                annotation_path=os.path.join(
                    self.project_root, "test_data/annotations/cvat/cvat-all-attributes.xml"
                ),
                image_shape=(500, 375),
                classifications=[],
                bounding_boxes=[],
                segmentations=[
                    Segmentation(
                        class_identifier=ClassIdentifier(class_id=2, class_name="car"),
                        model_class_identifier=ClassIdentifier(class_id=2, class_name="car"),
                        polygon=[
                            [115.31, 122.73],
                            [230.8, 136.51],
                            [232.77, 220.5],
                            [126.47, 244.78],
                            [95.63, 175.88],
                        ],
                        score=1.0,
                        difficult=False,
                        occluded=False,
                        background=False,
                        content="",
                        meta_attributes={},
                    )
                ],
                ocr_perception=None,
                image_dir=os.path.join(self.project_root, "test_data/images/dummy_task"),
                annotation_dir=os.path.join(self.project_root, "test_data/images/dummy_task"),
                replacement_string=os.path.join(self.project_root, "test_data/images/dummy_task"),
            ),
            BaseAnnotation(
                image_path=os.path.join(
                    self.project_root, "test_data/images/dummy_task/truck.jpg"
                ),
                annotation_path=os.path.join(
                    self.project_root, "test_data/annotations/cvat/cvat-all-attributes.xml"
                ),
                image_shape=(500, 375),
                classifications=[
                    Classification(
                        class_identifier=ClassIdentifier(class_id=2, class_name="car"),
                        model_class_identifier=ClassIdentifier(class_id=2, class_name="car"),
                        score=1.0,
                    ),
                    Classification(
                        class_identifier=ClassIdentifier(class_id=0, class_name="person"),
                        model_class_identifier=ClassIdentifier(class_id=0, class_name="person"),
                        score=1.0,
                    ),
                ],
                bounding_boxes=[
                    BoundingBox(
                        class_identifier=ClassIdentifier(class_id=0, class_name="person"),
                        model_class_identifier=ClassIdentifier(class_id=0, class_name="person"),
                        box=Box(xmin=28.33, ymin=9.28, xmax=130.62, ymax=78.5, angle=0.0),
                        score=1.0,
                        difficult=False,
                        occluded=False,
                        background=False,
                        content="car",
                        meta_attributes={"other": "test-box"},
                    )
                ],
                segmentations=[
                    Segmentation(
                        class_identifier=ClassIdentifier(class_id=0, class_name="person"),
                        model_class_identifier=ClassIdentifier(class_id=0, class_name="person"),
                        polygon=[
                            [115.31, 122.73],
                            [230.8, 136.51],
                            [232.77, 220.5],
                            [126.47, 244.78],
                            [95.63, 175.88],
                        ],
                        score=1.0,
                        difficult=False,
                        occluded=False,
                        background=False,
                        content="",
                        meta_attributes={"other": "test-polygon"},
                    )
                ],
                ocr_perception=None,
                image_dir=os.path.join(self.project_root, "test_data/images/dummy_task"),
                annotation_dir=os.path.join(self.project_root, "test_data/images/dummy_task"),
                replacement_string=os.path.join(self.project_root, "test_data/images/dummy_task"),
            ),
        ]

        assert annotations == expected_annotations

    def test_parse_from_cvat_all_attributes_no_difficult(self) -> None:
        yaml_config_path = os.path.join(
            self.project_root,
            "test_data/test_AnnotationHandler",
            "annotation-handler_cvat_all_attributes_test.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )
        annotation_handler.configuration.cvat_input_data[0].use_difficult = False

        annotations = annotation_handler.parse_annotations_from_cvat()

        assert len(annotations) == 4
        assert len(annotations[0].bounding_boxes) == 4
        assert len(annotations[1].bounding_boxes) == 0
        assert len(annotations[2].bounding_boxes) == 0
        assert len(annotations[3].bounding_boxes) == 1

        assert annotations[0].bounding_boxes[0].difficult is False
        assert annotations[0].bounding_boxes[0].occluded is False
        assert annotations[0].bounding_boxes[0].background is False
        assert annotations[0].bounding_boxes[0].content == "car"

        assert annotations[0].bounding_boxes[1].difficult is False
        assert annotations[0].bounding_boxes[1].occluded is False
        assert annotations[0].bounding_boxes[1].background is True
        assert annotations[0].bounding_boxes[1].content is ""

        assert annotations[0].bounding_boxes[2].difficult is False
        assert annotations[0].bounding_boxes[2].occluded is False
        assert annotations[0].bounding_boxes[2].background is False
        assert annotations[0].bounding_boxes[2].content is ""

        assert annotations[0].bounding_boxes[3].difficult is False
        assert annotations[0].bounding_boxes[3].occluded is True
        assert annotations[0].bounding_boxes[3].background is True
        assert annotations[0].bounding_boxes[3].content is ""

    def test_parse_from_cvat_all_attributes_no_occluded(self) -> None:
        yaml_config_path = os.path.join(
            self.project_root,
            "test_data/test_AnnotationHandler",
            "annotation-handler_cvat_all_attributes_test.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )
        annotation_handler.configuration.cvat_input_data[0].use_occluded = False

        annotations = annotation_handler.parse_annotations_from_cvat()

        assert len(annotations) == 4
        assert len(annotations[0].bounding_boxes) == 4
        assert len(annotations[1].bounding_boxes) == 0
        assert len(annotations[2].bounding_boxes) == 0
        assert len(annotations[3].bounding_boxes) == 1

        assert annotations[0].bounding_boxes[0].difficult is False
        assert annotations[0].bounding_boxes[0].occluded is False
        assert annotations[0].bounding_boxes[0].background is False
        assert annotations[0].bounding_boxes[0].content == "car"

        assert annotations[0].bounding_boxes[1].difficult is False
        assert annotations[0].bounding_boxes[1].occluded is False
        assert annotations[0].bounding_boxes[1].background is True
        assert annotations[0].bounding_boxes[1].content is ""

        assert annotations[0].bounding_boxes[2].difficult is True
        assert annotations[0].bounding_boxes[2].occluded is False
        assert annotations[0].bounding_boxes[2].background is False
        assert annotations[0].bounding_boxes[2].content is ""

        assert annotations[0].bounding_boxes[3].difficult is False
        assert annotations[0].bounding_boxes[3].occluded is False
        assert annotations[0].bounding_boxes[3].background is False
        assert annotations[0].bounding_boxes[3].content is ""

    def test_parse_from_cvat_all_attributes_no_background(self) -> None:
        yaml_config_path = os.path.join(
            self.project_root,
            "test_data/test_AnnotationHandler",
            "annotation-handler_cvat_all_attributes_test.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )
        annotation_handler.configuration.cvat_input_data[0].use_background = False

        annotations = annotation_handler.parse_annotations_from_cvat()

        assert len(annotations) == 4
        assert len(annotations[0].bounding_boxes) == 3
        assert len(annotations[1].bounding_boxes) == 0
        assert len(annotations[2].bounding_boxes) == 0
        assert len(annotations[3].bounding_boxes) == 1

        assert annotations[0].bounding_boxes[0].difficult is False
        assert annotations[0].bounding_boxes[0].occluded is False
        assert annotations[0].bounding_boxes[0].background is False
        assert annotations[0].bounding_boxes[0].content == "car"

        assert annotations[0].bounding_boxes[1].difficult is True
        assert annotations[0].bounding_boxes[1].occluded is False
        assert annotations[0].bounding_boxes[1].background is False
        assert annotations[0].bounding_boxes[1].content is ""

        assert annotations[0].bounding_boxes[2].difficult is False
        assert annotations[0].bounding_boxes[2].occluded is False
        assert annotations[0].bounding_boxes[2].background is False
        assert annotations[0].bounding_boxes[2].content is ""


if __name__ == "__main__":
    main()
