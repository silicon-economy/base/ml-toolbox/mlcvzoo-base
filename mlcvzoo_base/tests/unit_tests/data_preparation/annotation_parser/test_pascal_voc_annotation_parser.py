# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

import logging
import os
from unittest import main

from mlcvzoo_base.api.data.annotation import BaseAnnotation
from mlcvzoo_base.api.data.bounding_box import BoundingBox
from mlcvzoo_base.api.data.box import Box
from mlcvzoo_base.api.data.class_identifier import ClassIdentifier
from mlcvzoo_base.data_preparation.annotation_handler import AnnotationHandler
from mlcvzoo_base.tests.unit_tests.test_template import TestTemplate

logger = logging.getLogger(__name__)


class TestPascalVOCAnnotationParser(TestTemplate):
    def test_parse_from_pascal_voc_xml(self) -> None:
        logger.info(
            "############################################################\n"
            "# TEST parsing from pascal-voc xml:\n"
            "#      test_parse_from_pascal_voc_xml(self)\n"
            "############################################################"
        )

        yaml_config_path = os.path.join(
            self.project_root,
            "test_data/test_AnnotationHandler",
            "annotation-handler_pascal-voc_test.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )

        # TODO: add detailed check for parsed annotations
        annotations = annotation_handler.parse_annotations_from_xml()

    def test_parse_from_pascal_voc_xml_ignore_missing_images(self) -> None:
        logger.info(
            "############################################################\n"
            "# TEST parsing from pascal-voc xml:\n"
            "#      test_parse_from_pascal_voc_xml(self)\n"
            "############################################################"
        )

        yaml_config_path = os.path.join(
            self.project_root,
            "test_data/test_AnnotationHandler",
            "annotation-handler_pascal-voc_test_ignore_missing_images.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )

        # TODO: add detailed check for parsed annotations
        annotations = annotation_handler.parse_annotations_from_xml()

    def test_parse_from_pascal_voc_all_attributes(self) -> None:
        yaml_config_path = os.path.join(
            self.project_root,
            "test_data/test_AnnotationHandler",
            "annotation-handler_pascal-voc_all_attributes_test.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )

        annotations = annotation_handler.parse_annotations_from_xml()
        expected_annotations = [
            BaseAnnotation(
                image_path=os.path.join(self.project_root, "test_data/images/dummy_task/cars.jpg"),
                annotation_path=os.path.join(
                    self.project_root, "test_data/annotations/pascal_voc_all_attributes/cars.xml"
                ),
                image_shape=(500, 375),
                classifications=[],
                bounding_boxes=[
                    BoundingBox(
                        class_identifier=ClassIdentifier(class_id=2, class_name="car"),
                        model_class_identifier=ClassIdentifier(class_id=2, class_name="car"),
                        box=Box(xmin=28.33, ymin=9.28, xmax=130.62, ymax=78.5, angle=0.0),
                        score=1.0,
                        difficult=False,
                        occluded=False,
                        background=False,
                        content="car",
                    ),
                    BoundingBox(
                        class_identifier=ClassIdentifier(class_id=2, class_name="car"),
                        model_class_identifier=ClassIdentifier(class_id=2, class_name="car"),
                        box=Box(xmin=32.73, ymin=146.4, xmax=129.3, ymax=211.22, angle=0.0),
                        score=1.0,
                        difficult=False,
                        occluded=False,
                        background=True,
                        content="",
                    ),
                    BoundingBox(
                        class_identifier=ClassIdentifier(class_id=2, class_name="car"),
                        model_class_identifier=ClassIdentifier(class_id=2, class_name="car"),
                        box=Box(xmin=34.5, ymin=246.05, xmax=128.41, ymax=325.42, angle=0.0),
                        score=1.0,
                        difficult=True,
                        occluded=False,
                        background=False,
                        content="",
                    ),
                    BoundingBox(
                        class_identifier=ClassIdentifier(class_id=2, class_name="car"),
                        model_class_identifier=ClassIdentifier(class_id=2, class_name="car"),
                        box=Box(xmin=30.53, ymin=357.16, xmax=125.77, ymax=433.88, angle=0.0),
                        score=1.0,
                        difficult=False,
                        occluded=False,
                        background=False,
                        content="",
                    ),
                    BoundingBox(
                        class_identifier=ClassIdentifier(class_id=2, class_name="car"),
                        model_class_identifier=ClassIdentifier(class_id=2, class_name="car"),
                        box=Box(xmin=199.4, ymin=48.08, xmax=321.54, ymax=145.08, angle=0.0),
                        score=1.0,
                        difficult=False,
                        occluded=True,
                        background=True,
                        content="",
                    ),
                ],
                segmentations=[],
                ocr_perception=None,
                image_dir=os.path.join(self.project_root, "test_data/images"),
                annotation_dir=os.path.join(
                    self.project_root, "test_data/annotations/pascal_voc_all_attributes"
                ),
                replacement_string=os.path.join(
                    self.project_root, "test_data/annotations/pascal_voc_all_attributes"
                ),
            ),
            BaseAnnotation(
                image_path=os.path.join(
                    self.project_root, "test_data/images/dummy_task/empty.jpg"
                ),
                annotation_path=os.path.join(
                    self.project_root, "test_data/annotations/pascal_voc_all_attributes/empty.xml"
                ),
                image_shape=(500, 375),
                classifications=[],
                bounding_boxes=[],
                segmentations=[],
                ocr_perception=None,
                image_dir=os.path.join(self.project_root, "test_data/images"),
                annotation_dir=os.path.join(
                    self.project_root, "test_data/annotations/pascal_voc_all_attributes"
                ),
                replacement_string=os.path.join(
                    self.project_root, "test_data/annotations/pascal_voc_all_attributes"
                ),
            ),
            BaseAnnotation(
                image_path=os.path.join(
                    self.project_root, "test_data/images/dummy_task/person.jpg"
                ),
                annotation_path=os.path.join(
                    self.project_root, "test_data/annotations/pascal_voc_all_attributes/person.xml"
                ),
                image_shape=(500, 375),
                classifications=[],
                bounding_boxes=[
                    BoundingBox(
                        class_identifier=ClassIdentifier(class_id=0, class_name="person"),
                        model_class_identifier=ClassIdentifier(class_id=0, class_name="person"),
                        box=Box(xmin=45.52, ymin=148.85, xmax=173.16, ymax=248.85, angle=0.0),
                        score=1.0,
                        difficult=False,
                        occluded=False,
                        background=False,
                        content="",
                        meta_attributes={"other": "test"},
                    )
                ],
                segmentations=[],
                ocr_perception=None,
                image_dir=os.path.join(self.project_root, "test_data/images"),
                annotation_dir=os.path.join(
                    self.project_root, "test_data/annotations/pascal_voc_all_attributes"
                ),
                replacement_string=os.path.join(
                    self.project_root, "test_data/annotations/pascal_voc_all_attributes"
                ),
            ),
            BaseAnnotation(
                image_path=os.path.join(
                    self.project_root, "test_data/images/dummy_task/truck.jpg"
                ),
                annotation_path=os.path.join(
                    self.project_root,
                    "test_data/annotations/pascal_voc_all_attributes/task_mlcvzoo unit test pascal voc_annotations_2024_07_09_11_15_10_pascal voc 1.1/Annotations/cars.xml",
                ),
                image_shape=(500, 375),
                classifications=[],
                bounding_boxes=[],
                segmentations=[],
                ocr_perception=None,
                image_dir=os.path.join(self.project_root, "test_data/images"),
                annotation_dir=os.path.join(
                    self.project_root, "test_data/annotations/pascal_voc_all_attributes"
                ),
                replacement_string=os.path.join(
                    self.project_root, "test_data/annotations/pascal_voc_all_attributes"
                ),
            ),
        ]

        assert expected_annotations == annotations

    def test_parse_from_pascal_voc_all_attributes_no_difficult(self) -> None:
        yaml_config_path = os.path.join(
            self.project_root,
            "test_data/test_AnnotationHandler",
            "annotation-handler_pascal-voc_all_attributes_test.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )
        annotation_handler.configuration.pascal_voc_input_data[0].use_difficult = False

        annotations = annotation_handler.parse_annotations_from_xml()

        assert len(annotations) == 4
        assert len(annotations[0].bounding_boxes) == 4
        assert len(annotations[1].bounding_boxes) == 0
        assert len(annotations[2].bounding_boxes) == 1
        assert len(annotations[3].bounding_boxes) == 0

        assert annotations[0].bounding_boxes[0].difficult is False
        assert annotations[0].bounding_boxes[0].occluded is False
        assert annotations[0].bounding_boxes[0].background is False
        assert annotations[0].bounding_boxes[0].content == "car"

        assert annotations[0].bounding_boxes[1].difficult is False
        assert annotations[0].bounding_boxes[1].occluded is False
        assert annotations[0].bounding_boxes[1].background is True
        assert annotations[0].bounding_boxes[1].content == ""

        assert annotations[0].bounding_boxes[2].difficult is False
        assert annotations[0].bounding_boxes[2].occluded is False
        assert annotations[0].bounding_boxes[2].background is False
        assert annotations[0].bounding_boxes[2].content == ""

        assert annotations[0].bounding_boxes[3].difficult is False
        assert annotations[0].bounding_boxes[3].occluded is True
        assert annotations[0].bounding_boxes[3].background is True
        assert annotations[0].bounding_boxes[3].content == ""

    def test_parse_from_pascal_voc_all_attributes_no_occluded(self) -> None:
        yaml_config_path = os.path.join(
            self.project_root,
            "test_data/test_AnnotationHandler",
            "annotation-handler_pascal-voc_all_attributes_test.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )
        annotation_handler.configuration.pascal_voc_input_data[0].use_occluded = False

        annotations = annotation_handler.parse_annotations_from_xml()

        assert len(annotations) == 4
        assert len(annotations[0].bounding_boxes) == 4
        assert len(annotations[1].bounding_boxes) == 0
        assert len(annotations[2].bounding_boxes) == 1
        assert len(annotations[3].bounding_boxes) == 0

        assert annotations[0].bounding_boxes[0].difficult is False
        assert annotations[0].bounding_boxes[0].occluded is False
        assert annotations[0].bounding_boxes[0].background is False
        assert annotations[0].bounding_boxes[0].content == "car"

        assert annotations[0].bounding_boxes[1].difficult is False
        assert annotations[0].bounding_boxes[1].occluded is False
        assert annotations[0].bounding_boxes[1].background is True
        assert annotations[0].bounding_boxes[1].content == ""

        assert annotations[0].bounding_boxes[2].difficult is True
        assert annotations[0].bounding_boxes[2].occluded is False
        assert annotations[0].bounding_boxes[2].background is False
        assert annotations[0].bounding_boxes[2].content == ""

        assert annotations[0].bounding_boxes[3].difficult is False
        assert annotations[0].bounding_boxes[3].occluded is False
        assert annotations[0].bounding_boxes[3].background is False
        assert annotations[0].bounding_boxes[3].content == ""

    def test_parse_from_pascal_voc_all_attributes_no_background(self) -> None:
        yaml_config_path = os.path.join(
            self.project_root,
            "test_data/test_AnnotationHandler",
            "annotation-handler_pascal-voc_all_attributes_test.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )
        annotation_handler.configuration.pascal_voc_input_data[0].use_background = False

        annotations = annotation_handler.parse_annotations_from_xml()

        assert len(annotations) == 4
        assert len(annotations[0].bounding_boxes) == 3
        assert len(annotations[1].bounding_boxes) == 0
        assert len(annotations[2].bounding_boxes) == 1
        assert len(annotations[3].bounding_boxes) == 0

        assert annotations[0].bounding_boxes[0].difficult is False
        assert annotations[0].bounding_boxes[0].occluded is False
        assert annotations[0].bounding_boxes[0].background is False
        assert annotations[0].bounding_boxes[0].content == "car"

        assert annotations[0].bounding_boxes[1].difficult is True
        assert annotations[0].bounding_boxes[1].occluded is False
        assert annotations[0].bounding_boxes[1].background is False
        assert annotations[0].bounding_boxes[1].content == ""

        assert annotations[0].bounding_boxes[2].difficult is False
        assert annotations[0].bounding_boxes[2].occluded is False
        assert annotations[0].bounding_boxes[2].background is False
        assert annotations[0].bounding_boxes[2].content == ""


if __name__ == "__main__":
    main()
