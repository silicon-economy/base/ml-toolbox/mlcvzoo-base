# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

import hashlib
import logging
import os
from unittest import main

from mlcvzoo_base.api.data.class_identifier import ClassIdentifier
from mlcvzoo_base.data_preparation.annotation_handler import AnnotationHandler
from mlcvzoo_base.data_preparation.annotation_writer.txt_annotation_writer import (
    ClassificationAnnotationWriter,
    TextCropAnnotationWriter,
)
from mlcvzoo_base.tests.unit_tests.test_template import TestTemplate

logger = logging.getLogger(__name__)


class TestTxtAnnotationWriter(TestTemplate):
    def test_write_to_txt_classification(self) -> None:
        yaml_config_path = os.path.join(
            self.project_root,
            "test_data/test_AnnotationHandler/"
            "annotation-handler_label-studio_classification_test.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )

        # create new dummy annotations per known annotations
        annotations = annotation_handler.parse_training_annotations()

        output_path = os.path.join(
            self.project_root,
            "test_output/data_preparation/annotation_writer/txt_classification_annotations.txt",
        )

        annotation_writer = ClassificationAnnotationWriter(
            output_file_path=output_path, data_root_dir=self.project_root, seperator=" "
        )

        annotation_writer.write(annotations=annotations)

        expected_annotation_lines = [
            "test_data/images/classification_test_dataset/black/black_image.jpg 0",
            "test_data/images/classification_test_dataset/blue/blue_image.jpg 1",
            "test_data/images/classification_test_dataset/red/red_image.jpg 2",
        ]

        with open(output_path, "r") as annotation_file:
            annotation_lines = annotation_file.readlines()

        annotation_lines = [line.strip() for line in annotation_lines]

        annotation_lines.sort()

        assert annotation_lines == expected_annotation_lines

    def test_write_to_txt_cropped_boxes(self) -> None:
        yaml_config_path = os.path.join(
            self.project_root,
            "test_data/test_AnnotationHandler/annotation-handler_label-studio_ocr_test.yaml",
        )

        annotation_handler = AnnotationHandler(
            yaml_config_path=yaml_config_path,
            string_replacement_map=self.string_replacement_map,
        )

        # create new dummy annotations per known annotations
        annotations = annotation_handler.parse_training_annotations()

        output_path = os.path.join(
            self.project_root,
            "test_output/data_preparation/annotation_writer/txt_ocr_annotations.txt",
        )

        annotation_writer = TextCropAnnotationWriter(
            output_file_path=output_path,
            data_root_dir=self.project_root,
            seperator=";",
            rel_image_output_dir=os.path.join("test_output/data_preparation/annotation_writer"),
        )

        annotation_writer.write(annotations=annotations)

        expected_annotation_lines = [
            (
                "test_output/data_preparation/annotation_writer/cars/"
                "ilu-code_0_hello-ilu-code.jpg;hello-ilu-code"
            ),
            (
                "test_output/data_preparation/annotation_writer/cars/"
                "license-plate_1_hello-license-plate.jpg;hello-license-plate"
            ),
            "test_output/data_preparation/annotation_writer/person/ilu-code_0_test.jpg;test",
            (
                "test_output/data_preparation/annotation_writer/truck/"
                "license-plate_1_test.jpg;test"
            ),
        ]

        with open(output_path, "r") as annotation_file:
            annotation_lines = annotation_file.readlines()

        annotation_lines = [line.strip() for line in annotation_lines]

        # Sort lines in order to have a deterministic behavior for tests
        annotation_lines.sort()

        assert annotation_lines == expected_annotation_lines


if __name__ == "__main__":
    main()
